# kak-stkmach

## _A Forth compiler in pure kakscript_

This is a partial implementation of [Forth](#forth-resources) in pure `kakscript`. It encapsulates `kakscript` intended and un-intended computational capabilities (loops, if-then-else, arithmetic, string comparison etc.) in a well-defined (Forth standard) wrapper, with as little overhead as currently possible.

[[_TOC_]]

## Install

Clone / symlink into `autoload`, or use a plugin manager:
```
plug https://gitlab.com/kstr0k/kak-stkmach
```


## Usage

### Boot a stack machine

`stkmach` can "boot" multiple machines, each with independent stacks, inputs and word definitions. The parameter to `stkmach-boot` acts as a **prefix to the API commands** (`*-eval`, `*-interact` etc) and options (`*_stack`); it must obey the regex `[a-zA-Z0-9_]+` (i.e. no dashes).

```
require-module stkmach
stkmach-boot ff   # create stack machine 'ff'
buffer *debug*    # output is shown in *debug* by default
#ff-config-debug  # show stack & input after each step in *debug*

# save keystrokes with some aliases
alias global ff|   ff-eval
alias global fff|  ff-eval-forth
alias global ffff| ff-eval-forth-file

ff| words  # show available words
```

The rest of this document assumes a machine named `ff` as in the snippet above; replace with the actual machine prefix if different.

### Basic operation

```
ff|  1 2 + .         # tokenized Forth; RPN ops: push numbers, add, print
fff| %{ 1 . ." X" }  # Forth program in string (prints "1 X")
ff-interact          # interactive buf; pass lines to ff-eval-forth
```

### Examples

Note that in Forth, loops and conditionals (`IF` &hellip; `ELSE` &hellip; `THEN`) are only available in definitions, so some examples define an "auxiliary" word (`ex`).

Try these at the `kak` prompt; or in an `...-interact` buffer (in normal Forth syntax, without `ff|` / `fff|` and without `\` escapes for "`;`"):

```
# define 'sqr' to square the top-of-stack (TOS): dup(licate) TOS, multiply
ff| : sqr dup * \;
ff| 4 sqr sqr .  # =256

# print squares; i = current loop index
ff| : ex 5 0 do i sqr . loop \; ex

# print odd integers in 0..9
ff| : odd? 2 mod \;
# non-zero means true; Forth THEN means ENDIF
ff| : ex 10 0 do i dup odd? if . else drop then loop \; ex

# print odds, negate evens; 0= inverts booleans
ff| : ex 10 0 do i dup odd? 0= if negate then . loop \; ex

# multiplication table; j = outer counter; needs the Forth parser!
fff| %{ : mt 10 2 do 10 2 do i . ." * " j . ." = " i j * . cr loop loop ; mt }

# recursive factorial
ff| : fac-r dup 1 > if dup 1- recurse * then \;
```

### Constants, variables, values, arrays

```
ff| 5 constant c
ff| c .  # 'c' is now a word

ff| 6 value val
ff| val .
ff| 7 to val
ff| val .

ff| variable var
ff| 7 var !
ff| var @ .  # 7
ff| 7 var +!
ff| var ?    # 14

ff| create arr
ff| 3 cells allot      # arr+0 .. arr+2
ff| 7 arr 2 cells + !  # arr[2]=7
ff| arr 2 cells + @ .  # 7
```

### Meta-programming

```
ff| : multiplier create , does> @ * \;

ff| 3 multiplier 3*
ff| 7 multiplier 7*
ff| 5 3* .  # 15
ff| 2 7* .  # 14
```


## Forth conformance

Call `ff| words` to see **available words**; `stkmach` currently implements *most* words from [core + core-ext](https://forth-standard.org/standard/core) and *some* from [double](https://forth-standard.org/standard/double), [exception](https://forth-standard.org/standard/exception) (`try`, no `catch`), [facility](https://forth-standard.org/standard/facility), [memory](https://forth-standard.org/standard/memory), [tools](https://forth-standard.org/standard/tools), [string](https://forth-standard.org/standard/string). Some highlights:

### Testing

`kak-stkmach` passes core-related (and some other) tests in the Forth 2012 testsuite, except those for unimplemented features. See the [`kak-stkmach-forth2012-test-suite`](https://gitlab.com/kstr0k/kak-stkmach-forth2012-test-suite).

### Implemented

- most of Core, including `CREATE` / `DOES>` meta-programming & vectored execution (`'` / `EXECUTE`, `DEFER` / `IS`)
- full compilation support, including
  - all looping and conditional / branching constructs (`[?]DO` &hellip; `LEAVE` &hellip; `[+]LOOP`, `BEGIN` &hellip; `WHILE` &hellip; `REPEAT` / `UNTIL`, `AHEAD`, `IF` &hellip; `ELSE` &hellip; `THEN`, `CASE` &hellip; `OF` &hellip;)
  - compiler / interpreter `STATE` management (`] .. [`); `IMMEDIATE`, `POSTPONE`
  - other compiler-only constructs (`[']`, `[CHAR]`, `LITERAL`)
- interpreted `[IF]` &hellip; `[ELSE]` &hellip; `[THEN]`
- Forth strings, comments etc (`{s|c|.}" ..."`)

### Missing

- `BASE` conversions: only `HEX` input; no pictured numbers
- no double-precision numbers (in particular, no `M*`, `FM/MOD`, `SM/REM`; some `D...` ops implemented though); no floats
- limited Forth parser &mdash; commands *other than `ff-eval-forth`* require pre-tokenized Forth. E.g. the semicolon that ends a definition must be quoted/escaped: `ff-eval : sqr dup * \;`. Case is sensitive by default (see `ff-config-case` below). `ff-eval` doesn't support Forth strings and comments.
- `ff-eval-forth[-file]` tokenizes Forth code from a string / file, handling regular Forth syntax (including Forth strings and comments), then calls `ff-eval`. It's hackish and possibly buggy. There is no `PARSE` or `WORD` (but there is `PARSE-NAME`).
- limited Forth I/O (only `.` `EMIT` `CR` `SPACE` `TYPE` `." ..."`; no `ACCEPT`/`KEY`)
- some arithmetic operations (in particular, `/` and `MOD`) overflow and give wrong results for an `INT32_MIN` dividend

### Extensions

- the stack can hold numbers and strings; to push strings onto the stack, use the `'' STR` builtin in `ff-eval` (that's an empty word, obvioulsy not callable via `ff-eval-forth`). There are string-op extentions (`\\kak\str+`, `\\kak\str=`, `\\kak\str<>`)
- `\\kak\:macro WORD`: define a macro (expanded verbatim in the input)
  - word definitions using primitives that pop from the input (e.g. word-defining words using `CREATE` & friends, DEFER / IS, `'` & `[']`, `CHAR` & `[CHAR]`) **only work if compiled** normally (since macro-expansion mixes up the original input and the definition contents).
  - macro-expanded recursive words act as if tail-call optimized (arbitrary recursion depth)


## API

- `ff-{input|stack}-set`, `ff-{input|stack}-push`, `ff-stack-push-rev`: add/assign arguments to `%opt{ff_stack|ff_input}`. Push pushes to the front.
- `ff-clear`: clear stack and input
- `ff-eval INPUT..`: clear internal state, evaluate until empty
- `ff-run INPUT..`: set `ff_input`, evaluate until empty
- `ff-eval-forth FORTH_PROG`: parse `FORTH_PROG`, send to `ff-eval`. Uses `stkmach-prog-forth2kak` internally (see docstring). There's also `ff-eval-forth-file FORTH_FILE`.
- `ff-step`: step through `ff_input` once (`ff-run` calls this repeatedly)

### Configuration

Use `ff-config PARAM [...]` (also available as `ff-config-PARAM` with appropriate completions):
- `ff-config-flush-print {true|false}`: by default (`false`), printing is buffered and the output is flushed only when evaluation terminates; you can turn auto-flush on (though in `*debug*` output mode, every print will cause a newline).
- `ff-config-print METHOD [DESTINATION]`: output to
  - the `*debug*` buffer (`debug`, `debug-kkq` &mdash; the latter kakoune-quoted)
  - nowhere (`null`)
  - an info-box (`info`); info boxes are transient, which won't work well with `...-flush-print true`
  - a `str` or `str-list` option (`opt OPTNAME`)
  - a regular buffer (`buf BUFNAME`)
  - a file (append: `file-append FNAME`; overwrite on each print: `file-overwrite FNAME`); `file-append` calls `sh` on each flush.
  - anywhere &mdash; set `%opt{ff_config_print_code}` manually
- `ff-config-verbose N`: set verbosity level for `stkmach` information messages printed to `*debug*` (0 &hellip; 7, like Linux Kernel log levels). Default: 4 (WARNING) when booting, then 5 (NOTICE).
- `ff-config-case N`: 0 = case sensitive, -1 = case insensitive (slower); `ff-eval-forth` automatically sets & restores case insensitivity
- `ff-config-debug`: output to `*debug*`, set `-verbose` to 7, and also show the stack & input after each interpreter step. You can also set `%opt{ff_config_debug_step}` manually.


## Interfacing with Kakoune

- `\\kak\eval{|-stk|-top}`: pop the TOS string and evaluate it in Kakoune, with no args / the (remaining) stack / the (remaining) TOS.
- `\\kak\expand`: pop TOS, expand, push onto stack. Try `ff| '' '%val{buflist}' \\kak\expand .s`
- `\\kak\sh`: pop TOS, evaluate with `%sh`, push output

### stkmach as supercharged kakscript

```
# Forth: add all numbers on stack: call '+' depth-1 times
ff-eval : sum depth 1- 0 do + loop \;
decl int sum
def sum -params .. %{  # wrap Forth in kakscript command
  ff-stack-set %arg{@}
  ff-eval sum
  set global sum %opt{ff_stack}
}
sum 3 4 5
echo -debug %opt{sum}

# join strings on stack with delimiter (kak-string extensions)
ff| : sjoin depth 1- 0 do '' -DELIM- swap \\kak\str+ \\kak\str+ loop \;
ff-clear; ff-stack-push-rev dashes 'all the way' joined
ff| sjoin
echo -debug %opt{ff_stack}
# 'dashes-DELIM-all the way-DELIM-joined'
```


## Implementation

The codebase uses load-time `kak` macros via [`stkmach_loader`](rc/stkmach-kkslib.kak). The current machine (e.g. `ff`) is `@R@`.

- each word, whether builtin or colon-defined, has a corresponding integer "execution token" (XT) as well as a `kak` alias (e.g. `+` gets `@R@-impl-+` and might get XT 113). In the codebase, `@R@-stkimpl-def` assigns XTs and aliases.
- both the interpreter and compiler look up XTs for words using the `-impl-` aliases
- for each XT there are `kak` commands (e.g. `@R@-xtcode-113`, which performs the actual addition) and properties (`@R@_xtprop_113_PROPNAME` options). `xtcode` commands have a docstring pointing to the original name, which may help debugging.
- `xtcode` commands need the stack passed as an argument list

Colon-defined words trigger a trampoline loop driver (`@R@-driver1`). This loop handles unstructured jumps via exceptions and the return stack (`@R@_rstk`):
1. pop the XT on top of `@R@_rstk`
2. if -1, stop (driver scope exhausted; the -1 marker is pushed initially before entering the loop); otherwise, call the XT
3. if an exception occurs, check if it is `@R@-driver1-jump-exc`. If so, repeat from step 1; otherwise pass the error through

You may have noticed that it's impossible to return from a trampoline exception-jump. In order to emulate subroutine calls, the compiler splits up the code at every call-point into two XTs, pre-call and post-call: it terminates the current XT, creates another XT for the post-call "continuation", pushes it onto the return stack, then pushes the destination. The effect is that the driver jumps to the destination, then, when the subroutine inevitably `EXIT`'s, it goes on to the continuation. Every colon-definition contains an implicit `EXIT` at the end (and may also contain explicit `EXIT`s). `EXIT` is just a jump to the XT on top of the return stack.

To see these in action, try `ff-config-verbose 7`, then:
```
:ff| : ex if 10 . then 11 . \;
\ ff-msg[5]: compiled: ex = ff-xtcode-237
\ ff-msg[7]: compiled: def ff-xtcode-237 %{ ff-driver1-0jump-to 238;ff-stack-push 10;ff-xtcode-48 %opt{ff_stack};ff-driver1-jump-to 238; }
\ ff-msg[7]: compiled: def ff-xtcode-238 %{ ff-stack-push 11;ff-xtcode-48 %opt{ff_stack}; }
```

Thus, `ex` is XT 237. XT 48 is Forth builtin `.` (print). `ex` tests the TOS and conditionally (`0jump`) executes or skips some code (`10 .`); either way, it finally jumps to XT 238.

In an `IF ELSE THEN` scenario,
```
:ff| : ex2 if 10 . else 9 . then 11 . \;
\ ff-msg[5]: compiled: ex2 = ff-xtcode-239
\ ff-msg[7]: compiled: def ff-xtcode-239 %{ ff-driver1-0jump-to 240;ff-stack-push 10;ff-xtcode-48 %opt{ff_stack};ff-driver1-jump-to 241;ff-driver1-jump-to 240; }
\ ff-msg[7]: compiled: def ff-xtcode-240 %{ ff-stack-push 9;ff-xtcode-48 %opt{ff_stack};ff-driver1-jump-to 241; }
\ ff-msg[7]: compiled: def ff-xtcode-241 %{ ff-stack-push 11;ff-xtcode-48 %opt{ff_stack}; }
```

Here `ex2` is XT 239. If the TOS is 0 (false), the driver jumps to the `ELSE` part (XT 240); otherwise, it executes `10 .` and jumps to XT 241 (there is some dead code after the jump, since `stkmach` has no optimizer). XT 241 runs on both sides of the test and prints 11.

Subroutine call (splitting the Forth "thread"):
```
:ff| : ex3 20 . ex 30 . \;
\ ff-msg[5]: compiled: ex3 = ff-xtcode-243
\ ff-msg[7]: compiled: def ff-xtcode-243 %{ ff-stack-push 20;ff-xtcode-48 %opt{ff_stack};ff-driver1-call 237 244; }
\ ff-msg[7]: compiled: def ff-xtcode-244 %{ ff-stack-push 30;ff-xtcode-48 %opt{ff_stack}; }
```

Note the call to `ff-driver1-call` with destination XT 237 (`ex`) and return XT 244 (the post-call continuation); it simply pushes 237 and 244 on the return stack, than raises `@R@-driver1-jump-exc`.


## Forth resources

- [Forth Standard](https://forth-standard.org/standard/)
- [Starting Forth](https://www.forth.com/starting-forth)
- [Forth primer by J. V. Noble](http://galileo.phys.virginia.edu/classes/551.jvn.fall01/primer.htm)
- [Gforth manual](https://gforth.org/manual/)
- [Wikipedia: Forth](https://en.wikipedia.org/wiki/Forth_(programming_language))
- The ["Moving Forth" series](http://www.bradrodriguez.com/papers/moving1.htm), for implementers

## Copyright

`Alin Mr. <almr.oss@outlook.com>` / UNLICENSE
