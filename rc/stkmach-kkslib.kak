try %{ nop %opt{stkmach_loader_del_buf} } catch %{
  decl -hidden -docstring %{
    Pre-declare to 'nop' to keep loader buffers
  } str-list stkmach_loader_del_buf delete-buffer
}
decl -hidden str stkmach_loader_code_str
def stkmach_loader -params 4 -docstring %{
  Preprocess source code, then eval it
  Args: prefix suffix code preprocessor
} %{
  eval -no-hooks %{
    edit -scratch  # WARN: won't work if called from require-module
    eval -draft -save-regs 'abcdefghijklmnopqrstuvwxyz"@^:/|' %{
      reg dquote %arg{3}; exec '%"_d' 'p'
      eval %arg{4}
      exec '%'; set global stkmach_loader_code_str %reg{dot}
    }
    %opt{stkmach_loader_del_buf}
  }
  eval "%arg{1}%opt{stkmach_loader_code_str}%arg{2}"
} -override -hidden

try     %{ nop -- %arg{-2147483648}  # GCC-compiled kak bug
  decl -hidden str stkmach_kks_intgt0_mcode %{eval nop -- "%%arg{<c-r>1}"; def -override -hidden -params "0..<c-r>1" stkmach-kks-tmp-parses-ge0 ''}
} catch %{
  decl -hidden str stkmach_kks_intgt0_mcode %{eval nop -- "%%arg{<c-r>1}"}
}

def stkmach_loader_do_fmt %{
  # select & save all lines not ending in FMT:KEEP
  exec -save-regs '' '%S' '[^\n]*  #FMT:KEEP\n' <ret>Z
  try %{ exec zs '\s+#[^\n]*' <ret> '"_d' }  # eat comments + preceding space, including \n (!)
  try %{ exec zs '(?<=\s)[^\S\n]+' <ret> '"_d' }  # eat non-newline spaces, preceded by space or \n
# try %{ exec zs ^$  '<ret>"_d' }  # breaks ...\<\n><empty-line-or-comment>
  # remove all preprocessing directives, if any
  try %{ exec '%s' '  #FMT:[^\n]*' <ret> '"_d' }
} -override -hidden

def stkmach_loader_do_macros %{
  try %{ exec '%s' \b DEFPOVH [^\S\n]+ <ret>c 'DEFP -hidden ' <esc> }
  try %{ exec '%s' \b DEFP [^\S\n]+ (.*?\S+) [^\S\n]* '\[([0-9.-]+)\]' <ret>c %{def -override <c-r>1 -params <c-r>2} <esc> }

  try %{ exec '%s' \b REP-OPT-2 [^\S\n]+ (\S+) <ret>c %{set -add global <c-r>1 %opt{<c-r>1}} <esc> }

  try %{ exec \
    '%s' \b SLIST-POP-CHK-1_(\d) [^\S\n]+ (\S+) [^\S\n]+ (\S+) <ret>c \
    %{!S!-slist-pop-chk-1_<c-r>1-aux <c-r>2 <c-r>3 %opt{<c-r>2}} <esc>
  }

  try %{ exec '%s' \b INT-ADDSUB      [^\S\n]+ (\S+) [^\S\n]+ (\S+) [^\S\n]+ '([^\s;]+)' <ret>c 'INT-ADDSUB-INTO <c-r>1 <c-r>2 <c-r>3 !S!_int' <esc> }
  try %{ exec '%s' \b INT-ADDSUB-INTO [^\S\n]+ (\S+) [^\S\n]+ (\S+) [^\S\n]+ (\S+) [^\S\n]+ '([^\s;]+)' <ret>c 'SETG= <c-r>4 <c-r>2; set <c-r>1 global <c-r>4 <c-r>3' <esc> }

  try %{ exec '%s' \b SETG[=] [^\S\n]+ <ret>c 'set global ' <esc> }
  try %{ exec '%s' \b SETG[+] [^\S\n]+ <ret>c 'set -add global ' <esc> }
  try %{ exec '%s' \b SETG[-] [^\S\n]+ <ret>c 'set -remove global ' <esc> }

  try %{ exec '%s' \b INTGT0 [^\S\n]+ '([^\s;]+)' <ret>c %opt{stkmach_kks_intgt0_mcode} <esc> }
} -override -hidden

def stkmach_loader_do_kks_pfx %{
  exec '%s' '!S!-'  <ret>c 'stkmach-kks-' '<esc>'
  exec '%s' '!S!_'  <ret>c 'stkmach_kks_' '<esc>'
  try %{
  exec '%s' '!S!:-' <ret>c 'stkmach-kks'  '<esc>'
  }
  try %{
  exec '%s' '!S!:_' <ret>c 'stkmach_kks'  '<esc>'
  }
} -override -hidden

def stkmach_loader_do_all %{
  stkmach_loader_do_fmt
  stkmach_loader_do_macros
  stkmach_loader_do_kks_pfx
} -override -hidden

stkmach_loader '' '' %{

provide-module !S!-lib %{
## kakscript lib

DEFPOVH !S!-str-def-literal[1] %{
  def -params 0 -override -hidden "!S!-str-literal-%arg{1}-%arg{1}" %{}
}
DEFPOVH !S!-str-is-literal[2]  %{
  "!S!-str-literal-%arg{1}-%arg{2}"
}

decl -hidden str !S!_char_newline %{
}
decl -hidden str !S!_char_tab %{	}
decl -hidden str !S!_char_lbrace   '{'
decl -hidden str !S!_char_rbrace   '}'
decl -hidden str !S!_char_percnt   '%'
decl -hidden str !S!_char_quot     '"'
decl -hidden str !S!_char_apos     "'"
decl -hidden str !S!_char_semi     ';'
decl -hidden str !S!_char_bsol     '\'

decl -hidden str !S!_int64_max   9223372036854775807
decl -hidden str !S!_int64_min  -9223372036854775808
decl -hidden str !S!_int63_max   4611686018427387903
decl -hidden str !S!_int63_min  -4611686018427387904
decl -hidden str !S!_int32_max   2147483647
decl -hidden str !S!_int32_min  -2147483648
decl -hidden str !S!_int31_max   1073741823
decl -hidden str !S!_int31_min  -1073741824
decl -hidden str !S!_int16_max   32767
decl -hidden str !S!_int16_min  -32768
decl -hidden str !S!_int15_max   16383
decl -hidden str !S!_int15_min  -16384

!S!-str-def-literal %opt{!S!_int64_max}
!S!-str-def-literal %opt{!S!_int64_min}
!S!-str-def-literal %opt{!S!_int63_max}
!S!-str-def-literal %opt{!S!_int63_min}
!S!-str-def-literal %opt{!S!_int32_max}
!S!-str-def-literal %opt{!S!_int32_min}
!S!-str-def-literal %opt{!S!_int31_max}
!S!-str-def-literal %opt{!S!_int31_min}
!S!-str-def-literal %opt{!S!_int16_max}
!S!-str-def-literal %opt{!S!_int16_min}
!S!-str-def-literal %opt{!S!_int15_max}
!S!-str-def-literal %opt{!S!_int15_min}

!S!-str-def-literal ''
!S!-str-def-literal '-'
!S!-str-def-literal '_'
!S!-str-def-literal '--'
!S!-str-def-literal true
!S!-str-def-literal false
!S!-str-def-literal on
!S!-str-def-literal off
!S!-str-def-literal yes
!S!-str-def-literal no
!S!-str-def-literal get
!S!-str-def-literal set
!S!-str-def-literal add
!S!-str-def-literal sub
!S!-str-def-literal remove
!S!-str-def-literal def
!S!-str-def-literal eq
!S!-str-def-literal not
!S!-str-def-literal fail
!S!-str-def-literal error
!S!-str-def-literal if
!S!-str-def-literal then
!S!-str-def-literal else
!S!-str-def-literal 0
!S!-str-def-literal 1
!S!-str-def-literal 2
!S!-str-def-literal 8
!S!-str-def-literal 10
!S!-str-def-literal 16
!S!-str-def-literal 256
!S!-str-def-literal 1024
!S!-str-def-literal -1
!S!-str-def-literal 0x
!S!-str-def-literal 0b
!S!-str-def-literal 00

# list-length checkers
DEFPOVH !S!-nop-0_0[0..0] %{}
DEFPOVH !S!-nop-0_1[0..1] %{}
DEFPOVH !S!-nop-1_1[1..1] %{}
DEFPOVH !S!-nop-1_[1..]  %{}

DEFPOVH !S!-args-length-between-offby2[..] -docstring %{
  Fail unless MIN <= (length(ARG..) + 2) <= MAX
  Args: MIN MAX ARG..
  - MIN, MAX can be ''
} %{
  def !S!-args-length-cmp-tmp -params "%arg{1}..%arg{2}" -override -hidden ''
  !S!-args-length-cmp-tmp %arg{@}
}
DEFPOVH !S!-args-length-eq-offby1[..] -docstring %{
  Args: EXPECTED ARG..
  Fail unless (length(ARG..) + 1) == EXPECTED
} %{
  def !S!-args-length-cmp-tmp -params "%arg{1}..%arg{1}" -override -hidden ''
  !S!-args-length-cmp-tmp %arg{@}
}
DEFPOVH !S!-args-length-lt[..] -docstring %{
  Args: EXPECTED ARG..
  Asserts: length(ARG..) < EXPECTED
} %{
  def !S!-args-length-cmp-tmp -params "..%arg{1}" -override -hidden ''
  !S!-args-length-cmp-tmp %arg{@}
}
DEFPOVH !S!-args-length-ge-offby1[..] -docstring %{
  Args: EXPECTED ARG..
  Asserts: (length(ARG..) + 1) >= EXPECTED
} %{
  def !S!-args-length-cmp-tmp -params "%arg{1}.." -override -hidden ''
  !S!-args-length-cmp-tmp %arg{@}
}

decl -hidden str-list !S!_test_err_slist
decl -hidden str-list !S!_test_slist
decl -hidden str-list !S!_action_slist
decl -hidden str      !S!_code_str
decl -hidden str      !S!_str
decl -hidden str      !S!_str1
decl -hidden str      !S!_str2
decl -hidden str      !S!_arg1
decl -hidden str      !S!_arg2
decl -hidden str      !S!_arg3
decl -hidden str      !S!_arg4
decl -hidden int      !S!_int
decl -hidden int      !S!_int1
decl -hidden int      !S!_int2
decl -hidden int      !S!_cmp_int

# if-then-else support
DEFPOVH !S!-true[2] %{
  eval -- %arg{1}
}
DEFPOVH !S!-01if-1[2] %{
  eval -- %arg{1}
}
DEFPOVH !S!-false[2] %{
  eval -- %arg{2}
}
DEFPOVH !S!-01if-0[2] %{
  eval -- %arg{2}
}
DEFPOVH !S!-if[3] %{
  "!S!-%arg{1}" %arg{2} %arg{3}
}
DEFPOVH !S!-01if[3] %{
  "!S!-01if-%arg{1}" %arg{2} %arg{3}
}
DEFPOVH !S!-ifnot[3] %{
  "!S!-%arg{1}" %arg{3} %arg{2}
}
DEFPOVH !S!-01ifnot[3] %{
  "!S!-01if-%arg{1}" %arg{3} %arg{2}
}

decl -hidden str !S!_if_decision
DEFPOVH !S!-if-decision-eval[0] %{
  eval -- %opt{!S!_if_decision}
}
DEFPOVH !S!-if-decide-true[2] %{
  SETG= !S!_if_decision %arg{1}
}
DEFPOVH !S!-if-decide-false[2] %{
  SETG= !S!_if_decision %arg{2}
}
DEFPOVH !S!-if-decide[3] %{
  "!S!-if-decide-%arg{1}" %arg{2} %arg{3}
}

# catch-this support
DEFPOVH !S!-err-chk[..] %{
  SETG= !S!_test_err_slist %arg{@}
  SETG- !S!_test_err_slist %val{error}
  %opt{!S!_test_err_slist}
}
DEFPOVH !S!-err-eq[1] %{
  SETG= !S!_action_slist fail -- %val{error}
  try %{
    "!S!-str-literal-%arg{1}-%val{error}"
  } catch %{
    %opt{!S!_action_slist}
  }
}
DEFPOVH !S!-rethrow[0] %{ fail -- %val{error} }
DEFPOVH !S!-err-def[1] %{
  def %arg{1} -params .. -override -hidden %{fail -- %val{error}}
  !S!-str-def-literal %arg{1}
}

# error <-> boolean support
!S!-err-def !S!-fails-cmd-err
DEFPOVH !S!-failsnot-cmd-errret[..] %{
  try %{%arg{@}} catch %{ fail false }
  fail true
}
DEFPOVH !S!-fails-cmd-errret[..] %{
  try %{%arg{@}} catch %{ fail true }
  fail false
}
DEFPOVH !S!-fails-cmd[..] %{
  try %{
    %arg{@}
    SETG= !S!_action_slist fail !S!-fails-cmd-err
  } catch %{
    SETG= !S!_action_slist
  }
  %opt{!S!_action_slist}
}
DEFPOVH !S!-fails-cond-throw[3] -docstring %{
  try %arg{3}; fail with %arg{1} on error, %arg{2} on success
} %{
  try %arg{3} catch %{
    fail -- %arg{1}
  }
  fail -- %arg{2}
}
DEFPOVH !S!-failsnot-cond-throw[3] -docstring %{
  try %arg{3}; fail with %arg{1} on success, %arg{2} on error
} %{
  try %arg{3} catch %{
    fail -- %arg{2}
  }
  fail -- %arg{1}
}

# str comparison

!S!-err-def !S!-strcmp-err

DEFPOVH !S!-streq0[1] %{
  "!S!-str-literal--%arg{1}"
}
DEFPOVH !S!-streq0-errret[1] %{
  !S!-failsnot-cmd-errret "!S!-str-literal--%arg{1}"
}
DEFPOVH !S!-strne0[1] %{
  !S!-fails-cmd !S!-streq0 %arg{1}
}
DEFPOVH !S!-strne0-errret[1] %{
  !S!-fails-cmd-errret    "!S!-str-literal--%arg{1}"
}
DEFPOVH !S!-strne[2] %{
  SETG= !S!_test_slist %arg{1}
  SETG- !S!_test_slist %arg{2}
  !S!-nop-1_1 %opt{!S!_test_slist}
}
DEFPOVH !S!-streq[2] %{
  SETG= !S!_test_slist %arg{1}
  SETG- !S!_test_slist %arg{2}
  !S!-nop-0_0 %opt{!S!_test_slist}
}
DEFPOVH !S!-strne-orfail[2] %{
  try %{ !S!-strne %arg{1} %arg{2} } catch %{ fail !S!-strcmp-err }
}
DEFPOVH !S!-streq-orfail[2] %{
  try %{ !S!-streq %arg{1} %arg{2} } catch %{ fail !S!-strcmp-err }
}
DEFPOVH !S!-strne-errret[2] %{
  !S!-failsnot-cmd-errret !S!-strne %arg{1} %arg{2}
}
DEFPOVH !S!-streq-errret[2] %{
  !S!-failsnot-cmd-errret !S!-streq %arg{1} %arg{2}
}

# int ops
# TODO: some of these may not check for int args
# TODO: 00, 01, -0

DEFPOVH !S!-int-addsub-into[4] %{
  INT-ADDSUB-INTO %arg{1} %arg{2} %arg{3} %arg{4}
}
DEFPOVH !S!-int-addsub[3] %{
  INT-ADDSUB-INTO %arg{1} %arg{2} %arg{3} !S!_int
}

DEFPOVH !S!-int-is[1] %{ SETG= !S!_int %arg{1} }
DEFPOVH !S!-int-neg[1] %{
  SETG= !S!_int 0
  SETG- !S!_int %arg{1}
}
DEFPOVH !S!-inteq0[1] %{
  "!S!-str-literal-0-%arg{1}"
}
DEFPOVH !S!-inteq1[1] %{
  "!S!-str-literal-1-%arg{1}"
}
DEFPOVH !S!-intne0[1] %{
  !S!-fails-cmd "!S!-str-literal-0-%arg{1}"
}
DEFPOVH !S!-intne1[1] %{
  !S!-fails-cmd !S!-inteq1 %arg{1}
}
DEFPOVH !S!-inteq0-errret[1] %{
  !S!-failsnot-cmd-errret "!S!-str-literal-0-%arg{1}"
}
DEFPOVH !S!-intne0-errret[1] %{
  !S!-fails-cmd-errret    "!S!-str-literal-0-%arg{1}"
}
DEFPOVH !S!-intgt0[1] %{
  INTGT0 %arg{1}
}
DEFPOVH !S!-intge0[1] %{
  def -override -hidden -params "0..%arg{1}" !S!-tmp-parses-ge0 ''
}
DEFPOVH !S!-intlt0[1] %{
  !S!-fails-cmd !S!-intge0 %arg{1}
}
DEFPOVH !S!-intle0[1] %{
  !S!-fails-cmd INTGT0 %arg{1}
}
DEFPOVH !S!-inteq[2] %{
  INT-ADDSUB-INTO -remove %arg{1} %arg{2} !S!_cmp_int
  "!S!-str-literal-0-%opt{!S!_cmp_int}"
}
DEFPOVH !S!-intne[2] %{
  INT-ADDSUB-INTO -remove %arg{1} %arg{2} !S!_cmp_int
  !S!-fails-cmd "!S!-str-literal-0-%opt{!S!_cmp_int}"
}
# WARN: careful, int{g|l}{t|e} internally subtract, which may cause wrap-around
DEFPOVH !S!-intge[2] %{
  INT-ADDSUB-INTO -remove %arg{1} %arg{2} !S!_cmp_int
  !S!-intge0 %opt{!S!_cmp_int}
}
DEFPOVH !S!-intle[2] %{
  INT-ADDSUB-INTO -remove %arg{2} %arg{1} !S!_cmp_int
  !S!-intge0 %opt{!S!_cmp_int}
}
DEFPOVH !S!-intgt[2] %{
  INT-ADDSUB-INTO -remove %arg{1} %arg{2} !S!_cmp_int
  INTGT0 %opt{!S!_cmp_int}
}
DEFPOVH !S!-intlt[2] %{
  INT-ADDSUB-INTO -remove %arg{2} %arg{1} !S!_cmp_int
  INTGT0 %opt{!S!_cmp_int}
}
# wrap-around safe
decl -hidden str-list !S!_intg_w_code \
  try     %{ INTGT0 %arg{2}
    !S!-intgt-w-aux1 %arg{@}
  } catch %{
    !S!-intgt-w-aux2 %arg{@}
  }
DEFPOVH !S!-intgt-w[2] %{
  %opt{!S!_intg_w_code}
  INTGT0 %opt{!S!_cmp_int}
}
decl -hidden str-list !S!_intl_w_code \
  try     %{ INTGT0 %arg{1}
    !S!-intgt-w-aux1 %arg{2} %arg{1}
  } catch %{
    !S!-intgt-w-aux2 %arg{2} %arg{1}
  }
DEFPOVH !S!-intlt-w[2] %{
  %opt{!S!_intl_w_code}
  INTGT0 %opt{!S!_cmp_int}
}
DEFPOVH !S!-intge-w[2] %{
  %opt{!S!_intg_w_code}
  !S!-intge0 %opt{!S!_cmp_int}
}
DEFPOVH !S!-intle-w[2] %{
  %opt{!S!_intl_w_code}
  !S!-intge0 %opt{!S!_cmp_int}
}
DEFPOVH !S!-intgt-w-aux1[2] -docstring %{
  Invariant: %arg{2} > 0
} %{
  try     %{ INTGT0 %arg{1}
    INT-ADDSUB-INTO -remove %arg{1} %arg{2} !S!_cmp_int
  } catch %{ # %arg{1} <= 0
    SETG= !S!_cmp_int -1
  }
}
DEFPOVH !S!-intgt-w-aux2[2] -docstring %{
  Invariant: %arg{2} <= 0
} %{
  try     %{ INTGT0 %arg{1}
    SETG= !S!_cmp_int 1
  } catch %{ # %arg{1} <= 0
    try     %{ "!S!_intgt_w_aux2_chk_%arg{1}_%arg{2}"
    } catch %{
      INT-ADDSUB-INTO -remove %arg{1} %arg{2} !S!_cmp_int
    }
  }
}
def -hidden -override -docstring %{
  Check for (0 - int32_min), which would overflow
} "!S!_intgt_w_aux2_chk_0_%opt{!S!_int32_min}" %{
  SETG= !S!_cmp_int 1
}


decl -hidden int !S!_abs_sgn_int
DEFPOVH !S!-abs-sgn-into[3] %{
  try %{
    INTGT0 %arg{1}
    SETG= %arg{2} %arg{1}
    SETG= %arg{3} ''
  } catch %{
    INT-ADDSUB-INTO -remove 0 %arg{1} !S!_abs_sgn_int
    INTGT0 %opt{!S!_abs_sgn_int}
    SETG= %arg{2} %opt{!S!_abs_sgn_int}
    SETG= %arg{3} -
  } catch %{
    SETG= %arg{2} 0
    SETG= %arg{3} ''
  }
}

DEFPOVH !S!-min-max-into[4] %{
  try %{
    !S!-intlt %arg{1} %arg{2}
    SETG= %arg{3} %arg{1}
    SETG= %arg{4} %arg{2}
  } catch %{
    SETG= %arg{3} %arg{2}
    SETG= %arg{4} %arg{1}
  }
}
# wrap-around safe
DEFPOVH !S!-min-max-w-into[4] -docstring %{
  Args: INT1 INT2 MIN_OPTNAME MAX_OPTNAME
} %{
  try %{
    !S!-intlt-w %arg{1} %arg{2}
    SETG= %arg{3} %arg{1}
    SETG= %arg{4} %arg{2}
  } catch %{
    SETG= %arg{3} %arg{2}
    SETG= %arg{4} %arg{1}
  }
}

decl -hidden str-list !S!_r4_base2
decl -hidden int !S!_base2_left
decl -hidden str-list !S!_int2base2_action
decl -hidden int      !S!_int2base2_p2
DEFPOVH !S!-int2base2[1] %{
  SETG= !S!_r4_base2
  SETG= !S!_base2_left %arg{1}
  SETG= !S!_int2base2_p2 1
  !S!-int2base2-aux 1
}
DEFPOVH !S!-int2base2-aux[1] %{
  REP-OPT-2 !S!_int2base2_p2
  try %{
    !S!-intlt %opt{!S!_base2_left} %opt{!S!_int2base2_p2}
    SETG= !S!_int2base2_action
  } catch %{
    # un-nest recursion
    SETG= !S!_int2base2_action !S!-int2base2-aux %opt{!S!_int2base2_p2}
  }
  %opt{!S!_int2base2_action}
  SETG- !S!_base2_left %arg{1}
  try %{
    !S!-intge0 %opt{!S!_base2_left}
    SETG= !S!_r4_base2 1 %opt{!S!_r4_base2}
  } catch %{
    SETG+ !S!_base2_left %arg{1}
    SETG= !S!_r4_base2 0 %opt{!S!_r4_base2}
  }
}
decl -hidden int !S!_r4_base22int
decl -hidden int !S!_base22int_p2
DEFPOVH !S!-base22int[0] %{
  SETG= !S!_r4_base22int 0
  SETG= !S!_base22int_p2 1
  try %{
    !S!-loop-inf !S!-base22int-aux
  } catch %{ !S!-err-eq !S!-stop }
}
DEFPOVH !S!-base22int-aux[0] %{
  try %{
    SLIST-POP-CHK-1_1 !S!_r4_base2 !S!_arg
  } catch %{ fail !S!-stop }
  !S!-01if %opt{!S!_arg1} %{
    SETG+ !S!_r4_base22int %opt{!S!_base22int_p2}
  } %{}
  REP-OPT-2 !S!_base22int_p2
}

# TODO: WARN: mod and abs convert to positive first, overflowing for @R@_int32_min

decl -hidden int !S!_mod_abs1
decl -hidden int !S!_mod_abs2
decl -hidden str !S!_mod_sgn1
decl -hidden str !S!_mod_sgn2
DEFPOVH !S!-natural-mod[2] %{
  SETG= !S!_r4_base2
  SETG= !S!_base2_left %arg{1}
  SETG= !S!_int2base2_p2 %arg{2}
  !S!-int2base2-aux %arg{2}
}
decl -hidden int !S!_r4_mod
DEFPOVH !S!-int-mod[2] %{
  try %{ !S!-intne0 %arg{2} } catch %{ fail %{!S!:-: division by 0} }
  !S!-abs-sgn-into %arg{1} !S!_mod_abs1 !S!_mod_sgn1
  !S!-abs-sgn-into %arg{2} !S!_mod_abs2 !S!_mod_sgn2
  !S!-sgn-mul-into %opt{!S!_mod_sgn1} %opt{!S!_mod_sgn2} !S!_mod_sgn1
  !S!-natural-mod %opt{!S!_mod_abs1} %opt{!S!_mod_abs2}
  "!S!-int-mod-handle%opt{!S!_mod_sgn1}"
  SETG= !S!_r4_mod "%opt{!S!_mod_sgn2}%opt{!S!_base2_left}"
}
DEFPOVH !S!-int-mod-handle[0] %{}
DEFPOVH !S!-int-mod-handle-[0] %{
  try %{
    !S!-inteq0 %opt{!S!_base2_left}
  } catch %{
    SETG= !S!_base2_left "-%opt{!S!_base2_left}"
    SETG+ !S!_base2_left %opt{!S!_mod_abs2}
  }
}

decl -hidden int !S!_r4_div
DEFPOVH !S!-int-div[2] %{
  try     %{ "!S!-str-literal-1-%arg{2}"
    SETG= !S!_r4_mod 0
    SETG= !S!_r4_div %arg{1}
  } catch %{
    !S!-int-mod %arg{@}
    !S!-base22int
    "!S!-int-div-handle%opt{!S!_mod_sgn1}"
  }
}
DEFPOVH !S!-int-div-handle[0]  %{
  SETG= !S!_r4_div %opt{!S!_r4_base22int}
}
DEFPOVH !S!-int-div-handle-[0] %{
  try %{
    !S!-inteq0 %opt{!S!_r4_mod}
    SETG= !S!_r4_div 0
  } catch %{
    SETG= !S!_r4_div -1
  }
  SETG- !S!_r4_div %opt{!S!_r4_base22int}
}

DEFPOVH !S!-sgn-mul-into[3] %{
  "!S!-sgn-mul-handle%arg{1}%arg{2}" %arg{3}
}
DEFPOVH !S!-sgn-mul-handle[1] %{
  SETG= %arg{1} ''
}
DEFPOVH !S!-sgn-mul-handle--[1] %{
  SETG= %arg{1} ''
}
DEFPOVH !S!-sgn-mul-handle-[1] %{
  SETG= %arg{1} '-'
}

decl -hidden int !S!_r4_mul
decl -hidden int !S!_mul_int2_times_p2
decl -hidden int !S!_mul_abs1
decl -hidden int !S!_mul_abs2
decl -hidden str !S!_mul_sgn1
decl -hidden str !S!_mul_sgn2
DEFPOVH !S!-int-mul[2] %{
  !S!-abs-sgn-into %arg{1} !S!_mul_abs1 !S!_mul_sgn1
  !S!-abs-sgn-into %arg{2} !S!_mul_abs2 !S!_mul_sgn2
  !S!-min-max-into %opt{!S!_mul_abs1} %opt{!S!_mul_abs2} !S!_mul_abs1 !S!_mul_abs2
  !S!-natural-mul %opt{!S!_mul_abs1} %opt{!S!_mul_abs2}
  !S!-sgn-mul-into %opt{!S!_mul_sgn1} %opt{!S!_mul_sgn2} !S!_mul_sgn1
  try %{
    SETG= !S!_r4_mul "%opt{!S!_mul_sgn1}%opt{!S!_r4_mul}"
  } catch %{
    # if !S!_r4_mul wrapped around, ignore the '--'
  }
}
DEFPOVH !S!-natural-mul[2] %{
  SETG= !S!_mul_int2_times_p2 %arg{2}
  SETG= !S!_r4_mul 0
  !S!-int2base2 %arg{1}
  try %{
    !S!-loop-256 !S!-natural-mul-aux
  } catch %{ !S!-err-eq !S!-stop }
}
DEFPOVH !S!-natural-mul-aux[0] %{
  try %{
    SLIST-POP-CHK-1_1 !S!_r4_base2 !S!_arg
  } catch %{ fail !S!-stop }
  !S!-01if %opt{!S!_arg1} %{SETG+ !S!_r4_mul %opt{!S!_mul_int2_times_p2}} %{}
  REP-OPT-2 !S!_mul_int2_times_p2
}

decl -hidden int !S!_r4_lshift
decl -hidden int !S!_lshift_cnt
DEFPOVH !S!-int-lshift[2] %{
  SETG= !S!_r4_lshift %arg{1}
  SETG= !S!_lshift_cnt %arg{2}
  try %{
    !S!-loop-256 eval %{
      try %{ INTGT0 %opt{!S!_lshift_cnt} } catch %{ fail !S!-stop }
      SETG+ !S!_lshift_cnt -1
      REP-OPT-2 !S!_r4_lshift
    }
  } catch %{ !S!-err-eq !S!-stop }
}

# str-list ops

decl -hidden str-list !S!_member_slist1
decl -hidden str-list !S!_member_slist2
DEFPOVH !S!-args-member1[..] %{
  !S!-args-member1-aux %arg{@}
  !S!-nop-1_ %opt{!S!_member_slist1}
}
DEFPOVH !S!-args-member1-not[..] %{
  !S!-args-member1-aux %arg{@}
  !S!-nop-0_0 %opt{!S!_member_slist1}
}
DEFPOVH !S!-args-member1-aux[..] %{
  # extract the hay (%arg{@:2})
  SETG= !S!_member_slist1 %arg{@}
  SETG- !S!_member_slist1 %arg{1}

  # slist2 = hay - needle
  SETG= !S!_member_slist2 %opt{!S!_member_slist1}
  SETG- !S!_member_slist2 %arg{1}
  # check if slist1 = slist2
  SETG- !S!_member_slist1 %opt{!S!_member_slist2}
}
DEFPOVH !S!-optlen-member[3] -docstring %{
  Args: HAYNAME HAYLEN NEEDLE
  Is NEEDLE a member of %opt{HAYNAME} with known length HAYLEN?
} %{
  eval "SETG= !S!_member_slist1 %%opt{%arg{1}}"
  SETG- !S!_member_slist1 %arg{3}
  # if nothing removed, throw
  !S!-args-length-eq-offby1 %arg{2} %opt{!S!_member_slist1}
}
DEFPOVH !S!-optlen-adjoin[3] -docstring %{
  Args: HAYNAME HAYLEN_NAME NEEDLE
  Add NEEDLE to HAYNAME if missing (also increment HAYLEN if so)
  May re-order the str-list.
} %{
  SETG- %arg{1} %arg{3}
  try %{
    eval "!S!-args-length-eq-offby1 %%opt{%arg{2}} %%opt{%arg{1}}"
  } catch %{
    SETG+ %arg{2} 1
  }
  SETG+ %arg{1} %arg{3}
}

DEFPOVH !S!-slist-del-1_1[1] %{ eval "!S!-slist-del-1_1-aux %%arg{1} %%opt{%arg{1}}" }
DEFPOVH !S!-slist-del-1_2[1] %{ eval "!S!-slist-del-1_2-aux %%arg{1} %%opt{%arg{1}}" }
DEFPOVH !S!-slist-del-1_3[1] %{ eval "!S!-slist-del-1_3-aux %%arg{1} %%opt{%arg{1}}" }
DEFPOVH !S!-slist-del-1_4[1] %{ eval "!S!-slist-del-1_4-aux %%arg{1} %%opt{%arg{1}}" }
DEFPOVH !S!-slist-del-1_1-aux[2..] %{
  SETG- %arg{1} %arg{2}
}
DEFPOVH !S!-slist-del-1_2-aux[3..] %{
  SETG- %arg{1} %arg{2} %arg{3}
}
DEFPOVH !S!-slist-del-1_3-aux[4..] %{
  SETG- %arg{1} %arg{2} %arg{3} %arg{4}
}
DEFPOVH !S!-slist-del-1_4-aux[5..] %{
  SETG- %arg{1} %arg{2} %arg{3} %arg{4} %arg{5}
}

DEFPOVH !S!-slist-pop-chk-1_1[2] -docstring %{
  Args: OPTNAME DESTRUCTURING_PREFIX
}                                %{ eval "!S!-slist-pop-chk-1_1-aux %%arg{1} %%arg{2} %%opt{%arg{1}}" }
DEFPOVH !S!-slist-pop-chk-1_2[2] %{ eval "!S!-slist-pop-chk-1_2-aux %%arg{1} %%arg{2} %%opt{%arg{1}}" }
DEFPOVH !S!-slist-pop-chk-1_3[2] %{ eval "!S!-slist-pop-chk-1_3-aux %%arg{1} %%arg{2} %%opt{%arg{1}}" }
DEFPOVH !S!-slist-pop-chk-1_4[2] %{ eval "!S!-slist-pop-chk-1_4-aux %%arg{1} %%arg{2} %%opt{%arg{1}}" }
DEFPOVH !S!-slist-pop-chk-1_1-aux[3..] %{
  SETG- %arg{1} %arg{2} %arg{3}
  SETG= "%arg{2}1" %arg{3}
}
DEFPOVH !S!-slist-pop-chk-1_2-aux[4..] %{
  SETG- %arg{1} %arg{2} %arg{3} %arg{4}
  SETG= "%arg{2}1" %arg{3}
  SETG= "%arg{2}2" %arg{4}
}
DEFPOVH !S!-slist-pop-chk-1_3-aux[5..] %{
  SETG- %arg{1} %arg{2} %arg{3} %arg{4} %arg{5}
  SETG= "%arg{2}1" %arg{3}
  SETG= "%arg{2}2" %arg{4}
  SETG= "%arg{2}3" %arg{5}
}
DEFPOVH !S!-slist-pop-chk-1_4-aux[6..] %{
  SETG- %arg{1} %arg{2} %arg{3} %arg{4} %arg{5} %arg{6}
  SETG= "%arg{2}1" %arg{3}
  SETG= "%arg{2}2" %arg{4}
  SETG= "%arg{2}3" %arg{5}
  SETG= "%arg{2}4" %arg{6}
}

decl -hidden int      !S!_r4_args_length
decl -hidden str-list !S!_args_length_slist
decl -hidden int      !S!_args_length_p2
decl -hidden str-list !S!_args_length_action
DEFPOVH !S!-args-length[..] %{
  SETG= !S!_r4_args_length 0
  SETG= !S!_args_length_slist %arg{@}
  SETG= !S!_args_length_p2 1
  !S!-args-length-aux 1
  SETG+ !S!_r4_args_length -1
}
DEFPOVH !S!-args-length-aux[1] %{
  REP-OPT-2 !S!_args_length_p2
  SETG= !S!_args_length_action
  try %{
    !S!-args-length-ge-offby1 %opt{!S!_args_length_p2} %opt{!S!_args_length_slist}
    SETG= !S!_args_length_action !S!-args-length-aux %opt{!S!_args_length_p2}
  }
  %opt{!S!_args_length_action}
  SETG+ !S!_r4_args_length %arg{1}
  try %{
    !S!-args-length-ge-offby1 %opt{!S!_r4_args_length} %opt{!S!_args_length_slist}
  } catch %{
    SETG- !S!_r4_args_length %arg{1}
  }
}
DEFPOVH !S!-args-length-errret[..] %{
  !S!-args-length %arg{@}
  fail -- %opt{!S!_r4_args_length}
}

DEFPOVH !S!-arg2opt-offby1-errret[..] -docstring %{
  Args: IDX+1 LIST...
  Result: %val{error}
} %{
  eval "fail %%arg{%arg{1}}"
}
DEFPOVH !S!-args-1th-errret[..] %{ fail -- %arg{1} }
DEFPOVH !S!-args-2th-errret[..] %{ fail -- %arg{2} }
DEFPOVH !S!-args-3th-errret[..] %{ fail -- %arg{3} }
DEFPOVH !S!-args-4th-errret[..] %{ fail -- %arg{4} }
DEFPOVH !S!-args-5th-errret[..] %{ fail -- %arg{5} }
DEFPOVH !S!-args-6th-errret[..] %{ fail -- %arg{6} }
DEFPOVH !S!-arg2opt-offby2[3..] -docstring %{
  Args: OUT_OPT IDX+2 LIST...
} %{
  eval "SETG= %%arg{1} %%arg{%arg{2}}"
}
DEFPOVH !S!-args-1th-into[2..] %{
  SETG= %arg{1} %arg{2}
}
DEFPOVH !S!-args-2th-into[3..] %{
  SETG= %arg{1} %arg{3}
}
DEFPOVH !S!-args-3th-into[4..] %{
  SETG= %arg{1} %arg{4}
}
DEFPOVH !S!-args-4th-into[5..] %{
  SETG= %arg{1} %arg{5}
}
DEFPOVH !S!-args-5th-into[6..] %{
  SETG= %arg{1} %arg{6}
}
DEFPOVH !S!-args-6th-into[7..] %{
  SETG= %arg{1} %arg{7}
}

decl -hidden str      !S!_r4_strcat
decl -hidden str-list !S!_strcat_slist
DEFPOVH !S!-strcat[..] %{
  SETG= !S!_r4_strcat ''
  !S!-strcat-aux %arg{@}
}
DEFPOVH !S!-strcat-aux[..] %{
  SETG= !S!_strcat_slist %arg{@}
  !S!-while-do %{ SLIST-POP-CHK-1_1 !S!_strcat_slist !S!_arg
  } %{
    SETG+ !S!_r4_strcat %opt{!S!_arg1}
  }
}

DEFPOVH !S!-push1[..] %{
  eval "SETG= %%arg{1} %%arg{2} %%opt{%arg{1}}"
}

# looping support

SETG= !S!_code_str %{eval "SETG+ %%arg{1} %%opt{%arg{1}}";}
DEFPOVH !S!-rep-opt-2[1] %opt{!S!_code_str}
!S!-rep-opt-2 !S!_code_str
DEFPOVH !S!-rep-opt-4[1] %opt{!S!_code_str}
!S!-rep-opt-2 !S!_code_str
DEFPOVH !S!-rep-opt-16[1] %opt{!S!_code_str}
!S!-rep-opt-2 !S!_code_str
DEFPOVH !S!-rep-opt-256[1] %opt{!S!_code_str}

SETG= !S!_code_str %{%arg{@};}
DEFPOVH !S!-loop-1[..] %opt{!S!_code_str}
!S!-rep-opt-2 !S!_code_str
DEFPOVH !S!-loop-2[..] %opt{!S!_code_str}
!S!-rep-opt-2 !S!_code_str
DEFPOVH !S!-loop-4[..] %opt{!S!_code_str}
!S!-rep-opt-4 !S!_code_str
DEFPOVH !S!-loop-16[..] %opt{!S!_code_str}
!S!-rep-opt-16 !S!_code_str
DEFPOVH !S!-loop-256[..] %opt{!S!_code_str}

DEFPOVH !S!-loop-inf[..] %{
%arg{@};%arg{@};%arg{@};%arg{@};%arg{@};%arg{@};%arg{@};%arg{@}
!S!-loop-16 %arg{@};!S!-loop-16 %arg{@};!S!-loop-16 %arg{@};!S!-loop-16 %arg{@}
!S!-loop-16 !S!-loop-256 %arg{@}
!S!-loop-inf !S!-loop-256 !S!-loop-256 %arg{@}
fail %{loop-inf: shouldn't reach}
}
!S!-err-def !S!-stop

# TODO: nestable
!S!-err-def !S!-while-do-stop
decl -hidden str-list !S!_while_do_chk
decl -hidden str-list !S!_while_do_exec
DEFPOVH !S!-while-do[2] -docstring %{
  Args: CHK EXEC
} %{
  SETG= !S!_while_do_chk  try %arg{1} catch %{ fail !S!-while-do-stop }
  SETG= !S!_while_do_exec !S!-loop-inf eval "%%opt{!S!_while_do_chk};%arg{2}"
  try %{ %opt{!S!_while_do_exec} } catch %{ !S!-err-eq !S!-while-do-stop }
}

decl -hidden str-list !S!_int_for_action
decl -hidden int      !S!_int_for_cnt
decl -hidden str-list !S!_int_for_chk
decl -hidden str-list !S!_int_for_inc
!S!-err-def !S!-int-for-stop
DEFPOVH !S!-int-for[..] -docstring %{
  Args: N0 CHK INC ACTION..
  - CHK, INC: must parse as command str-lists
} %{
  SETG= !S!_int_for_action %arg{@}
  SETG- !S!_int_for_action %arg{1} %arg{2} %arg{3}
  SETG= !S!_int_for_cnt    %arg{1}
  eval "SETG= !S!_int_for_chk %arg{2}"
  eval "SETG= !S!_int_for_inc %arg{3}"
  try %{
    !S!-loop-inf eval %{
      try %{ %opt{!S!_int_for_chk} %opt{!S!_int_for_cnt} } catch %{ fail !S!-int-for-stop }
      %opt{!S!_int_for_action} %opt{!S!_int_for_cnt}
      %opt{!S!_int_for_inc} !S!_int_for_cnt
    }
  } catch %{ !S!-err-eq !S!-int-for-stop }
}

decl -hidden str-list !S!_args_rev_arg_list
DEFPOVH !S!-args-rev[1..] -docstring %{
  Args: OUT_SLIST IN..
} %{
  !S!-args-length %arg{@}
  !S!-gen-seq %opt{!S!_r4_args_length} 1 -1 '%arg{' '}' !S!_args_rev_arg_list
  eval "SETG= %%arg{1} %opt{!S!_args_rev_arg_list}"
  SETG= !S!_args_rev_arg_list
}

decl -hidden str      !S!_gen_seq_lim
decl -hidden str      !S!_gen_seq_pfx
decl -hidden str      !S!_gen_seq_sfx
decl -hidden str      !S!_gen_seq_out
DEFPOVH !S!-gen-seq[6] -docstring %{
  Args: N0 LIM INC PFX SFX OUT_OPT
  - LIMIT: exclusive
} %{
  SETG= !S!_gen_seq_pfx %arg{4}
  SETG= !S!_gen_seq_sfx %arg{5}
  SETG= !S!_gen_seq_out %arg{6}
  SETG= %arg{6}
  try %{
    INTGT0 %arg{3}
    SETG= !S!_gen_seq_lim "!S!-intgt %arg{2}"
  } catch %{
    SETG= !S!_gen_seq_lim "!S!-intlt %arg{2}"
  }
  !S!-int-for %arg{1} %opt{!S!_gen_seq_lim} "!S!-set-delta-name -add global %arg{3}" !S!-gen-seq-aux
}
DEFPOVH !S!-gen-seq-aux[1] %{
  set -add global %opt{!S!_gen_seq_out} "%opt{!S!_gen_seq_pfx}%arg{1}%opt{!S!_gen_seq_sfx}"
}

DEFPOVH !S!-slist-add-into[1..] -docstring %{
  Args: OUT_OPTNAME [STR]..
  - OUT_OPTNAME can be a str, int, or (uselessly) a *-list
} %{
  decl -hidden str-list !S!_slist_add_slist %arg{@}
  SETG- !S!_slist_add_slist %arg{1}
  decl -hidden str-list !S!_slist_add_step SETG+ %arg{1}
  try %{ !S!-loop-inf eval %{
    try %{ SLIST-POP-CHK-1_1 !S!_slist_add_slist !S!_str } catch %{ fail !S!-stop }
    %opt{!S!_slist_add_step} %opt{!S!_str1}
  } } catch %{ !S!-err-eq !S!-stop
  }
}

# misc

DEFPOVH !S!-quote-kak-sed-into[..] -docstring %{
  Quotes %arg{2:} using sh + sed
  Args: OUT_OPT ..
} %{
  SETG= %arg{1} %sh{
    shift
    first=true
    for s; do
      if "$first"; then
        printf \'
      else printf \ \'
      fi
      first=false
      printf %s "$s" | sed -e "s/'/''/g"
      printf \'
    done
  }
}

decl -hidden str-list !S!_to_quote_slist
DEFPOVH !S!-quote-kak-echo-into[..] -docstring %{
  Quotes %arg{3:} using the builtin echo & %file{} expansion
  Args: OUT_OPT TMP_FILE ..
} %{
  SETG= !S!_to_quote_slist %arg{@}
  SETG- !S!_to_quote_slist %arg{1} %arg{2}
  echo -to-file %arg{2} -quoting kakoune %opt{!S!_to_quote_slist}
  eval "SETG= %%arg{1} %%file""%arg{2}"""
}

decl -hidden str-list !S!_time_cmd_error
decl str !S!_elapsed_time
def !S!-time-cmd -params .. -docstring %{
  Args: command str-list
  Out:
  - !S!_elapsed_time
  - !S!_time_cmd_error (str-list): 0 items (success), or 2 (fail %val{error}) if raised by %arg{@} (error not swallowed)
} %{
  eval %sh{
    save_t() {
      t=${EPOCHREALTIME:-$(date +%s.%N)}
      ms=${t#*.}; s=${t%%.*}
      s=$(( s - 1640988000 ))  # 2022-01-01
      ms=${ms%${ms#???}}
      t=$s$ms
    }
    save_t; t0=$t
    printf >"$kak_command_fifo" '%s; %s "\n"\n' \
      'try -- %{ %arg{@}; set global !S!_time_cmd_error } catch %{ set global !S!_time_cmd_error fail -- %val{error} }' \
      "echo -to-file %\"$kak_response_fifo\" '
'"
    IFS= read dummy <"$kak_response_fifo"
    save_t
    printf 'set global !S!_elapsed_time %s\n' $(( t - t0 ))
  }
  %opt{!S!_time_cmd_error}
} -override -command-completion
def !S!-time-cmd-dbg -params .. -docstring %{
  Like !S!-time-cmd, but also print elapsed time to *debug*
} %{
  try %{ !S!-time-cmd %arg{@} }
  echo -debug "stkmach: elapsed time=%opt{!S!_elapsed_time}ms for:" %arg{@}
  %opt{!S!_time_cmd_error}
} -override -command-completion

DEFPOVH !S!-set-delta-name[4] -docstring %{
  'set' with last two args swapped
} %{
  set %arg{1} %arg{2} %arg{4} %arg{3}
}

DEFPOVH !S!-exists-cmd[1] %{
  try %{
    alias global !S!-tmp-parses-alias "%arg{1}"
    unalias global !S!-tmp-parses-alias
  } catch %{ fail !S!-exists-cmd-err }
}
!S!-err-def !S!-exists-cmd-err

DEFPOVH !S!-def-checked-alias[2] %{
  alias global -- %arg{1} %arg{2}
  alias global -- "!S!-checked-alias-defined:%arg{1}" !S!-nop-0_0
}
DEFPOVH !S!-exists-checked-alias[1] %{
  try %{
    "!S!-checked-alias-defined:%arg{1}"
  } catch %{ fail !S!-exists-alias-err }
}
!S!-err-def !S!-exists-alias-err

decl -hidden str-list !S!_eval_lambda_slist
DEFPOVH !S!-eval-lambda[..] %{
  SETG= !S!_eval_lambda_slist %arg{@}
  SETG- !S!_eval_lambda_slist %arg{1}
  def !S!-eval-lambda-lambda -params .. %arg{1} -override -hidden
  !S!-eval-lambda-lambda %opt{!S!_eval_lambda_slist}
}

DEFPOVH !S!-gensym-def[1] %{
decl -hidden int "%arg{1}_gensym_cnt" 1
eval "def -override -hidden -params 0 %arg{1}-gensym %%{
  SETG+ %arg{1}_gensym_cnt 1
}"
eval "def -override -hidden -params 2 %arg{1}-gensym-pfx-into %%{
  SETG= %%arg{2} ""%arg{1}_%%arg{1}%%opt{%arg{1}_gensym_cnt}""
  %arg{1}-gensym
}"
eval "def -override -hidden -params 1 %arg{1}-gensym-into %%{
  SETG= %%arg{1} %%opt{%arg{1}_gensym_cnt}
  %arg{1}-gensym
}"
}
!S!-gensym-def !S!:_

decl -hidden str      !S!_r4_cmd_namegen_def
DEFPOVH !S!-cmd-namegen-def[2] -docstring %{
  args: BODY DEF_FLAGS
  out var: !S!_r4_cmd_namegen_def
} %{
  !S!:_-gensym-pfx-into 'cmd_namegen' !S!_r4_cmd_namegen_def
  eval "def %%opt{!S!_r4_cmd_namegen_def} %%arg{1} %arg{2}"
}
DEFPOVH !S!-def-cmd-or-alias[3..] -docstring %{
  Args: NAME BODY DEF_FLAGS
  Result: !S!_r4_cmd_namegen_def = alias helper, or '' if no alias
} %{
  try %{
    eval "def %%arg{1} %%arg{2} %arg{3}"
    SETG= !S!_r4_cmd_namegen_def ''
  } catch %{
    !S!-cmd-namegen-def %arg{2} %arg{3}
    alias global %arg{1} %opt{!S!_r4_cmd_namegen_def}
  }
}

decl -hidden str-list !S!_batch_def_slist
decl -hidden str      !S!_batch_def_flags
DEFPOVH !S!-batch-def[..] -docstring %{
  Define functions/aliases in batch
  Args: NAME_PFX NAME_SFX DEF_FLAGS [NAME BODY]..
} %{
  SETG= !S!_batch_def_slist %arg{@}
  SETG- !S!_batch_def_slist %arg{1} %arg{2} %arg{3}
  SETG= !S!_batch_def_flags %arg{3}
  try %{
    !S!-loop-inf !S!-batch-def-aux1 %arg{1} %arg{2}
  } catch %{ !S!-err-eq !S!-stop }
}
DEFPOVH !S!-batch-def-aux1[2] %{
  !S!-batch-def-aux2 %arg{@} %opt{!S!_batch_def_slist}
}
DEFPOVH !S!-batch-def-aux2[..] %{
  try %{ !S!-nop-4_ %arg{@} } catch %{ fail !S!-stop }
  !S!-def-cmd-or-alias "%arg{1}%arg{3}%arg{2}" %arg{4} %opt{!S!_batch_def_flags}
  SETG- !S!_batch_def_slist %arg{3} %arg{4}
}

# bootstrap

decl -hidden str-list !S!_arg_seq_0
decl -hidden str-list !S!_arg_seq_16
decl -hidden str-list !S!_arg_rev_seq_0
decl -hidden str-list !S!_arg_rev_seq_16
DEFPOVH !S!-tmp-cmd[1] %{
  SETG+ !S!_arg_seq_16 "%%arg{%arg{1}}"
  SETG= !S!_arg_rev_seq_16 "%%arg{%arg{1}}" %opt{!S!_arg_rev_seq_16}
  decl -hidden str-list "!S!_arg_seq_%arg{1}" %opt{!S!_arg_seq_16}
  decl -hidden str-list "!S!_arg_rev_seq_%arg{1}" %opt{!S!_arg_rev_seq_16}
  def "!S!-nop-%arg{1}_%arg{1}" -params "%arg{1}..%arg{1}" %{} -override -hidden
  def "!S!-nop-%arg{1}_"        -params "%arg{1}.."        %{} -override -hidden
  def "!S!-nop-0_%arg{1}"       -params "0..%arg{1}"       %{} -override -hidden
}
!S!-int-for 1 %{!S!-intgt 17} %{!S!-set-delta-name -add global 1} !S!-tmp-cmd

}  # end module

} %{
  stkmach_loader_do_all
}
