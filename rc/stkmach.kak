decl -hidden str stkmach_source %val{source}

try %{ nop %opt{stkmach_debug_timer} } catch %{
  decl -hidden str-list stkmach_debug_timer
}

provide-module stkmach %{
  require-module stkmach-core
  require-module stkmach-instance
}

provide-module stkmach-core %{
stkmach_loader '' '' %{

require-module stkmach-kks-lib

decl -hidden -docstring %{
  Words that parse another word from the input token stream.
  Only used for skipping in interpreter
} str-list stkmach_2word_kw '' \\kak\push
decl -hidden int stkmach_2word_kw_len
try %{
  !S!-args-length-errret %opt{stkmach_2word_kw}
} catch %{
  decl -hidden int stkmach_2word_kw_len %val{error}
}
def -params "%opt{stkmach_2word_kw_len}..%opt{stkmach_2word_kw_len}" -override -hidden \
  "!S!-nop-%opt{stkmach_2word_kw_len}_%opt{stkmach_2word_kw_len}" %{}

decl -hidden str-list !S!_forth_if_action
DEFPOVH !S!-forth-if[3] %{
  try %{
    "!S!-forth-if-handle-%arg{1}" %arg{2} %arg{3}
  } catch %{
    SETG= !S!_forth_if_action fail -- %val{error}
    try     %{  # handle-* known   -> error caused by eval
      !S!-exists-cmd "!S!-forth-if-handle-%arg{1}"
    } catch %{  # handle-* unknown -> true
      SETG= !S!_forth_if_action eval %arg{2}
    }
    %opt{!S!_forth_if_action}
  }
}
DEFPOVH !S!-forth-when[1..] %{
  SETG= !S!_forth_if_action %arg{@}
  SETG- !S!_forth_if_action %arg{1}
  !S!-forth-if %arg{1} %{%opt{!S!_forth_if_action}} %{}
}
DEFPOVH !S!-forth-if-handle-0    [2] %{ eval -- %arg{2} }
DEFPOVH !S!-forth-if-handle-1    [2] %{ eval -- %arg{1} }
DEFPOVH !S!-forth-if-handle--1   [2] %{ eval -- %arg{1} }

DEFPOVH !S!-forth-if-decide--1[2] %{ SETG= !S!_if_decision %arg{1} }
DEFPOVH !S!-forth-if-decide-1 [2] %{ SETG= !S!_if_decision %arg{1} }
DEFPOVH !S!-forth-if-decide-0 [2] %{ SETG= !S!_if_decision %arg{2} }
DEFPOVH !S!-forth-if-decide[3] %{
  try %{
    "!S!-forth-if-decide-%arg{1}" %arg{2} %arg{3}
  } catch %{
    SETG= !S!_if_decision %arg{2}
  }
}

decl -hidden int !S!_r4_1st_char_code
DEFPOVH !S!-1st-char-code[1] %{
  SETG= !S!_r4_1st_char_code %sh{ printf '%d' "'${1%${1#?}}" }
}

DEFPOVH !S!-str2ord[2] -docstring %{
  Args: ACTION STR
  - ACTION must parse as a str-list
  Result (passed to ACTION): CNT CODE..
} %{
  eval -no-hooks %{
    edit -scratch *!S!-work-tmp*
    eval -draft -save-regs '"^' %{
      reg dquote "%arg{2}"
      exec '%"_d' 'P'
      try %{ exec \
        '%s' (.) <ret>c %{ '''<c-r>1'} <esc> \
        '%s' " '''''" <ret>c " \''" <esc>
      }
      exec -save-regs '' '%' 6H  # ignore extraneous quoted newline
      eval "!S!-str2ord-aux %%arg{1} %reg{.}"
    }
  }
}
DEFPOVH !S!-str2ord-aux[..] %{
  eval %arg{1} %sh{
    shift; printf "$# "; [ "$#" = 0 ] || printf "%d " "$@"
  }
}

DEFPOVH !S!-lowercase[2] %{
  try %{ !S!-streq0 %arg{1}
    # special case since selection never empty
    SETG= %arg{2} %arg{1}
  } catch %{
    eval -no-hooks -draft -save-regs %{"^} %{
      edit -scratch *!S!-work-tmp*
      reg dquote %arg{1}
      exec -save-regs %{} '%"_d' P '%H' `d
      SETG= %arg{2} %reg{dquote}
    }
  }
}

decl -hidden str stkmach_prog_forth2kak_token1
decl -hidden str stkmach_prog_forth2kak_mode
decl -hidden str stkmach_prog_forth2kak_accum_str
decl -hidden str-list stkmach_r4_prog_forth2kak
DEFPOVH stkmach-prog-forth2kak[2] -docstring %{
  Args: OUT_STR_LIST_OPTNAME STR
} %{
  eval -no-hooks -save-regs '"^abcd' %{
    edit -scratch *stkmach-prog-forth2kak*
    eval -draft %{
      reg dquote "%arg{2}"
      exec '%"_d' 'P'

      try %{ exec '%s' '^[^\S]*' '\\[^\S]' '[^\n]*' '\n' <ret> %{"_d} }  # delete comment-only lines
      # WARN: TODO: this breaks things like 'POSTPONE \', 'IS \', but later forth2kak breaks them anyway
      try %{ exec '%s' '(?<=[^\S])\\[^\S]' '[^")\n]*' '(?=\n)' <ret> %{"_d} }  # delete "safe" comments

      exec 'gg' 'i <esc>' 'ge' 'a<ret><esc>'  # prepend space, append newline to buf

      # escape single-quotes
      try %{ exec '%s' %<'> <ret>c %<''> <esc> }
      # single-quote tokens
      try %{ exec \
        '%s' '(^|\s)\K\S+' <ret> \
        i %<'> <esc>a %<'> <esc>
      }

      # WS=whitespace  ->  <blank> + <'> + <WS> + <'>
      try %{ exec '%s' '(\s)' <ret>c %{ '<c-r>1' <esc> } }

      # separate '"' and ')' suffixes into separate tokens
      try %{ exec '%s' %{([")])' } <ret>c %{' '<c-r>1' } <esc> }

      exec -save-regs '' '%'
      eval "decl -hidden str-list stkmach_prog_forth2kak_token_slist %reg{.}"
      SETG= stkmach_prog_forth2kak_mode normal
      SETG= stkmach_r4_prog_forth2kak
      !S!-while-do %{
        SLIST-POP-CHK-1_1 stkmach_prog_forth2kak_token_slist stkmach_prog_forth2kak_token
      } %{
        "stkmach-prog-forth2kak-mode-%opt{stkmach_prog_forth2kak_mode}" %opt{stkmach_prog_forth2kak_token1}
      }
      delete-buffer *stkmach-prog-forth2kak*
      try %{ delete-buffer *!S!-work-tmp* }
      SETG= %arg{1} %opt{stkmach_r4_prog_forth2kak}
    }
  }
}
DEFPOVH stkmach-prog-forth2kak-is-ws[1] %{
  !S!-args-member1 %arg{1} ' ' '	' '
'
}
DEFPOVH stkmach-prog-forth2kak-is-str-start[1] %{
  # TODO: HACK: no mixed-case 'ABORT"'
  !S!-args-member1 %arg{1} '."' ',"' 's"' 'S"' 'c"' 'C"' 'abort"' 'ABORT"' '.('
}
DEFPOVH stkmach-prog-forth2kak-mode-normal [1] %{
  try     %{ stkmach-prog-forth2kak-is-ws %arg{1}
    # ignore
  } catch %{
    SETG= stkmach_prog_forth2kak_mode word
    SETG= stkmach_prog_forth2kak_accum_str %arg{1}
  }
}
DEFPOVH stkmach-prog-forth2kak-mode-word   [1] %{
  try   %{ stkmach-prog-forth2kak-is-ws %arg{1}
    SETG= !S!_action_slist stkmach-prog-forth2kak-mode-word-on-ws %arg{1}
  } catch %{
    SETG= !S!_action_slist SETG+ stkmach_prog_forth2kak_accum_str %arg{1}
  }
  %opt{!S!_action_slist}
}
DEFPOVH stkmach-prog-forth2kak-mode-word-on-ws[1] %{
    try %{
      "stkmach-prog-forth2kak-mode-word-handle-%opt{stkmach_prog_forth2kak_accum_str}"
    } catch %{
      try %{ stkmach-prog-forth2kak-is-str-start %opt{stkmach_prog_forth2kak_accum_str}
        !S!-lowercase %opt{stkmach_prog_forth2kak_accum_str} stkmach_prog_forth2kak_accum_str
        decl -hidden str stkmach_prog_forth2kak_str_start %opt{stkmach_prog_forth2kak_accum_str}
        try     %{ !S!-streq '.(' %opt{stkmach_prog_forth2kak_accum_str}
          decl -hidden str stkmach_prog_forth2kak_str_stop ')'
        } catch %{
          decl -hidden str stkmach_prog_forth2kak_str_stop '"'
        }
        SETG= stkmach_prog_forth2kak_accum_str ''
        SETG= stkmach_prog_forth2kak_mode str
      } catch %{
        SETG= stkmach_prog_forth2kak_mode normal
        SETG+ stkmach_r4_prog_forth2kak %opt{stkmach_prog_forth2kak_accum_str}  # emit
      }
    }
}
!S!-batch-def stkmach-prog-forth2kak-mode-word-handle- '' %{-override -hidden} \
  '\' %{
    SETG= stkmach_prog_forth2kak_mode comment
  } \
  '(' %{
    SETG= stkmach_prog_forth2kak_mode paren
  }

DEFPOVH stkmach-prog-forth2kak-mode-comment[1] %{
  try %{ !S!-streq %arg{1} %opt{!S!_char_newline}
    SETG= stkmach_prog_forth2kak_mode normal
  }
}
DEFPOVH stkmach-prog-forth2kak-mode-paren  [1] %{
  try %{
    "stkmach-prog-forth2kak-mode-paren-handle-%arg{1}"
  }
}
!S!-batch-def stkmach-prog-forth2kak-mode-paren-handle- '' %{-override -hidden} \
  ')' %{
    SETG= stkmach_prog_forth2kak_mode normal
  }
DEFPOVH stkmach-prog-forth2kak-mode-str    [1] %{
  try %{ !S!-streq %arg{1} %opt{stkmach_prog_forth2kak_str_stop}
    SETG= stkmach_prog_forth2kak_mode normal
    SETG+ stkmach_r4_prog_forth2kak \\kak\push %opt{stkmach_prog_forth2kak_accum_str} "\\kak\str-%opt{stkmach_prog_forth2kak_str_start}"
  } catch %{
    SETG+ stkmach_prog_forth2kak_accum_str %arg{1}
  }
}

DEFP stkmach-interact[1] -docstring %{
  Show & configure *...-interact* buffer
  Args: MACHINE
} %{
  edit -scratch "*%arg{1}-interact*"
  "%arg{1}-config-print-buf" "*%arg{1}-interact*"
  map buffer normal <a-ret> ": stkmach-interact-eval %arg{1}<ret>"
  map buffer insert <a-ret> "<esc>: stkmach-interact-eval %arg{1}<ret>i"
  eval -save-regs '"' %{
    reg dquote "
\ <a-ret>: %arg{1}-eval-forth current/selected lines (insert/normal mode)
"
    exec %{%"_d} 'P' 'gg' 'd' 'ge' 'o'
  }
}
DEFPOVH stkmach-interact-eval[1] %{
  eval -save-regs 'f' %{
    exec %{<a-x><a-_><space>} %{"fy} <a-:> ';'
    try     %{ exec -draft Ge %{s\S<ret>}
      "%arg{1}-print" "
%reg{f}"  # before end-of-buffer: copy input
    } catch %{  # end-of-buffer: add newline
      exec Ge %{"_d} o
    }
    "%arg{1}-print" '\ output
'
    try %{
      "%arg{1}-eval-forth" %reg{f}
      "%arg{1}-print" '
\ ok
'
    } catch %{
      "%arg{1}-print" "
\ error: %val{error}
"
    }
    "%arg{1}-flush-print"
  }
  exec '<esc>'  # clear status line
}

} %{
  stkmach_loader_do_all
}

}  # end module stkmach-core

provide-module stkmach-instance %{
require-module stkmach-core

decl -hidden str stkmach_boot_code %{

decl -hidden -docstring %{
  To be @R@-eval'd at boot end
} str-list @R@_boot_late_code \
  variable base 10 base ! \
  : decimal 10 base ! ';' \
  : hex     16 base ! ';'

# backwards compatibility
alias global @R@-forth-if !S!-forth-if

decl -docstring %{
  Forth parameter stack
} str-list @R@_stack
decl -docstring %{
  Forth return stack
} str-list @R@_rstk
decl -hidden -docstring %{
  Forth internal control stack
} str-list @R@_cstk
decl -hidden -docstring %{
  Forth internal definitions stack
} str-list @R@_dstk
decl -hidden -docstring %{
  Forth internal DO/LOOP stack
} str-list @R@_lstk
decl str-list @R@_input
decl str-list @R@_config_print_code
decl str-list @R@_config_debug_step

# hooks for debug messages; levels: like Linux printk
decl -hidden str-list @R@_msg0 nop --
decl -hidden str-list @R@_msg1 nop --
decl -hidden str-list @R@_msg2 nop --
decl -hidden str-list @R@_msg3 nop --
decl -hidden str-list @R@_msg4 nop --
decl -hidden str-list @R@_msg5 nop --
decl -hidden str-list @R@_msg6 nop --
decl -hidden str-list @R@_msg7 nop --

!S!-gensym-def @R@_mem
decl -hidden int @R@_alloc_base 1000000
decl -hidden int @R@_alloc_ptr  %opt{@R@_alloc_base}
decl -hidden int @R@_pad_size 90  # 16 * 5; Forth std: >=84
decl -hidden int @R@_pad_base %opt{@R@_alloc_base}
SETG- @R@_pad_base %opt{@R@_pad_size}

!S!-gensym-def @R@_xt
SETG= @R@_xt_gensym_cnt 1

decl -hidden str @R@_work_dir %sh{
  d=${XDG_CACHE_HOME:-$HOME/.cache}/kak/stkmach/@R@_
  [ -d "$d" ] || mkdir -p "$d"
  printf %s "$d"
}

# actually, words
decl -hidden str-list @R@_words
decl -hidden int      @R@_words_cnt 0
SETG= @R@_words

# tmp's
decl -hidden str-list @R@_action_slist
decl -hidden int @R@_depth

decl -hidden str @R@_stk_arg1
decl -hidden str @R@_stk_arg2
decl -hidden str @R@_stk_arg3
decl -hidden str @R@_stk_arg4

decl -hidden str @R@_input_arg1
decl -hidden str @R@_input_arg2

decl -hidden str @R@_xt

decl -hidden str-list @R@_char_code_slist

!S!-err-def @R@-skip-stop

decl -hidden str @R@_r4_quote
DEFPOVH @R@-quote[..] %{
  !S!-quote-kak-echo-into @R@_r4_quote "%opt{@R@_work_dir}/quote.tmp" %arg{@}
}

decl -hidden str @R@_r4_forth2str
DEFPOVH @R@-helper-forth2str[2] %{
  SETG= @R@_char_code_slist
  decl -hidden int @R@_forth2str_stop_ptr %arg{1}
  SETG+            @R@_forth2str_stop_ptr %arg{2}
  !S!-gen-seq %arg{1} %opt{@R@_forth2str_stop_ptr} 1 ' %opt{@R@_mem_' '}' @R@_char_code_slist
  eval "@R@-helper-forth2str-aux %opt{@R@_char_code_slist}"  # TODO: HACK: use own var
}
DEFPOVH @R@-helper-forth2str-aux[..] %{
  SETG= @R@_r4_forth2str %sh{ printf %s\\n "$@"  | awk '{ printf "%c", $1}' }
}

DEFP @R@-config[1..] %{
  decl -hidden str-list @R@_config_args %arg{@}
  SETG-                 @R@_config_args %arg{1}
  "@R@-config-%arg{1}" %opt{@R@_config_args}
}

DEFP @R@-config-verbose[1] -docstring %{
  Args: LEVEL (0..7)
  Print only messages of level <= LEVEL
} %{
  DEFPOVH @R@-config-verbose-aux[1] %{ SETG= "@R@_msg%arg{1}" echo -debug "\ @R@-msg[%arg{1}]:" }
  INT-ADDSUB -add %arg{1} 1
  !S!-int-for 0 "!S!-intgt %opt{!S!_int}" %{!S!-set-delta-name -add global 1} @R@-config-verbose-aux

  DEFPOVH @R@-config-verbose-aux[1] %{ SETG= "@R@_msg%arg{1}" nop -- }
  INT-ADDSUB -add %arg{1} 1
  !S!-int-for %opt{!S!_int} "!S!-intgt 8" %{!S!-set-delta-name -add global 1} @R@-config-verbose-aux
}
@R@-config-verbose 4

decl -hidden int @R@_config_case
DEFP @R@-config-case[1] -docstring %{
  Args: CASE_SENSITIVE (0 = true, -1 = false)
} %{
  SETG= @R@_config_case %arg{1}
  "@R@-config-case-%arg{1}"
}
DEFPOVH @R@-config-case-0[0] %{
  DEFPOVH @R@-word-case-hook[2] %{}
}
DEFPOVH @R@-config-case--1[0] %{
  DEFPOVH @R@-word-case-hook[2] %{
    !S!-lowercase %arg{@}
  }
}
@R@-config-case 0

DEFP @R@-print-debug[..] %{
  echo -debug -- %arg{@}
}
DEFP @R@-print-debug-kkq[..] %{
  echo -debug -quoting kakoune -- %arg{@}
}
DEFP @R@-print-file-append[..] %{
  nop -- %sh{set -ue; printf %s "$@" >> "$kak_opt_@R@_config_print_filename"}
}
DEFP @R@-print-buf[..] %{
  buffer %opt{@R@_config_print_bufname}
  eval -draft -save-regs '"' %{
    exec -save-regs %{} '%H' 'd'
    reg dquote "%reg{dquote}%arg{@}"
    exec 'P'
  }
}
DEFP @R@-print-info[..] %{
  info -title @R@-output -- "%arg{@}"
}
DEFP @R@-print-str-opt[..] %{
  !S!-strcat %arg{@}
  SETG+ @R@_config_print_optname %opt{!S!_r4_strcat}
}

DEFP @R@-config-flush-print[1] %{
  !S!-if %arg{1} %{
    DEFPOVH @R@-flush-print-auto[0] @R@-flush-print
  }              %{
    DEFPOVH @R@-flush-print-auto[0] %{}
  }
}

DEFP @R@-config-print[1..] -docstring %{
  Args: METHOD [PARAM]..
} %{
  decl -hidden str-list @R@_config_args %arg{@}
  SETG-                 @R@_config_args %arg{1}
  "@R@-config-print-%arg{1}" %opt{@R@_config_args}
} -shell-script-candidates %{ printf '%s\n' debug debug-kkq null buf file-append file-overwrite info opt }

DEFPOVH @R@-config-print-debug[0] %{
  SETG= @R@_config_print_code @R@-print-debug
}
DEFPOVH @R@-config-print-debug-kkq[0] %{
  SETG= @R@_config_print_code @R@-print-debug-kkq
}
DEFPOVH @R@-config-print-null[0] %{
  SETG= @R@_config_print_code nop --
}
DEFPOVH @R@-config-print-buf[1] %{
  decl -hidden str @R@_config_print_bufname %arg{1}
  SETG= @R@_config_print_code @R@-print-buf
}
DEFPOVH @R@-config-print-file-overwrite[1] %{
  SETG= @R@_config_print_code echo -to-file %arg{1} --
}
DEFPOVH @R@-config-print-file-append[1] %{
  decl -hidden str @R@_config_print_filename %arg{1}
  SETG= @R@_config_print_code @R@-print-file-append
}
DEFPOVH @R@-config-print-slist-opt[1] %{
  SETG= @R@_config_print_code set -add global %arg{1}
}
DEFPOVH @R@-config-print-str-opt[1] %{
  decl -hidden str @R@_config_print_optname %arg{1}
  SETG= @R@_config_print_code @R@-print-str-opt
}
DEFPOVH @R@-config-print-opt[1] %{
  try     %{ SETG= %arg{1} 1 2  # then str-list var
    @R@-config-print-slist-opt %arg{1}
    SETG= %arg{1}
  } catch %{  # else str var
    @R@-config-print-str-opt %arg{1}
  }
}
DEFPOVH @R@-config-print-info[0] %{
  SETG= @R@_config_print_code @R@-print-info
}

decl -hidden str-list @R@_print_output
DEFP @R@-print[..] %{
  SETG+ @R@_print_output %arg{@}
  @R@-flush-print-auto
}
DEFP @R@-flush-print[0] %{
  try     %{ !S!-nop-0_0 %opt{@R@_print_output}
    SETG= @R@_action_slist
  } catch %{
    SETG= @R@_action_slist eval %{
      !S!-strcat %opt{@R@_print_output}
      %opt{@R@_config_print_code} %opt{!S!_r4_strcat}
      SETG= @R@_print_output
    }
  }
  %opt{@R@_action_slist}
}

@R@-config-print-debug
@R@-config-flush-print false

DEFP @R@-config-debug[0] %{
  SETG= @R@_config_debug_step %opt{@R@_config_debug_step_default}
  @R@-config-verbose 7
  @R@-config-print-debug
  @R@-config-flush-print true
}

DEFP @R@-stack-show[0] %{
  @R@-print "@R@-stack: %opt{@R@_stack}
"
}

DEFP @R@-interact[0] %{ stkmach-interact @R@:- }

decl -hidden str-list @R@_config_debug_step_default eval %{
@R@-print-debug '> @R@-s' %opt{@R@_stack}
@R@-print-debug '  @R@-i' %opt{@R@_input}  #FMT:KEEP
}

DEFP @R@-stack-set [..] %{ SETG= @R@_stack %arg{@} }
DEFP @R@-input-set [..] %{ SETG= @R@_input %arg{@} }
DEFPOVH @R@-stack-popv[..] %{ @R@-stack-POPV %arg{@} }
DEFPOVH @R@-input-popv[..] %{ @R@-input-POPV %arg{@} }
DEFP @R@-stack-push[..] %{
  @R@-stack-PUSH %arg{@}
}
DEFP @R@-input-push[..] %{
  @R@-input-PUSH %arg{@}
}
decl -hidden str-list @R@_rev_slist
DEFP @R@-stack-push-rev[..] %{
  !S!-args-rev @R@_rev_slist %arg{@}
  @R@-stack-PUSH %opt{@R@_rev_slist}
  SETG= @R@_rev_slist
}

decl -hidden str      @R@_input_parsed_1
decl -hidden str      @R@_input_parsed_2
decl -hidden str-list @R@_input_parsed_2o
DEFPOVH @R@-input-parse1[0] %{
  @R@-input-parse1-aux %opt{@R@_input}
}
DEFPOVH @R@-input-parse1-aux[..] %{
  !S!-nop-1_ %arg{@}
  SETG= @R@_input_parsed_1 %arg{1}
  @R@-input-POPV %arg{1}
  # !S!-optlen-member stkmach_2word_kw %opt{stkmach_2word_kw_len} %arg{1} # too slow
  SETG- stkmach_2word_kw %arg{1}
  try %{
    "!S!-nop-%opt{stkmach_2word_kw_len}_%opt{stkmach_2word_kw_len}" %opt{stkmach_2word_kw}
    SETG= @R@_input_parsed_2o
  } catch %{
    SETG+ stkmach_2word_kw %arg{1}
    !S!-nop-2_ %arg{@}
    SETG= @R@_input_parsed_2o %arg{2}
    @R@-input-POPV %arg{2}
  }
}

DEFP @R@-clear[0] %{
  @R@-stack-set
  @R@-input-set
  @R@-clear-internal
}
DEFPOVH @R@-clear-internal[0] %{
  SETG= @R@_rstk
  SETG= @R@_cstk
  SETG= @R@_lstk
  SETG= @R@_dstk
  @R@_STATE-set 0
}

!S!-err-def @R@-input-stop
DEFP @R@-run[..] %{
  @R@-input-set %arg{@}
  @R@-process-input
}
DEFPOVH @R@-interpret-input[0] %{
  @R@_STATE-set 0
  @R@-process-input
}
decl -hidden str-list @R@_process_input @R@-step
decl -hidden str-list @R@_process_input_action
DEFPOVH @R@-process-input[0] %{
  try     %{
    !S!-loop-inf eval %{%opt{@R@_process_input}}
  } catch %{
    @R@-flush-print
    %opt{@R@_config_debug_step}
    try %{ delete-buffer *!S!-work-tmp* }
    try     %{ !S!-err-eq @R@-input-stop  # ignore
    } catch %{
      @R@-process-input-failure %val{error}
    }
  }
}
DEFPOVH @R@-process-input-failure[1] %{
    @R@-clear
    try %{
      eval "SETG= !S!_test_err_slist %arg{1}"
      SLIST-POP-CHK-1_3 !S!_test_err_slist !S!_arg
      !S!-str-is-literal @R@-throw %opt{!S!_arg1}
    } catch %{
      fail -- %arg{1}
    }
    SETG= !S!_arg1 ''
    try %{ !S!-strne0 %opt{!S!_arg3}
      SETG= !S!_arg3 ": %opt{!S!_arg3}"
    }
    try %{ "@R@-handle-exc-%opt{!S!_arg2}"
      SETG= !S!_arg1 ": %opt{!S!_arg1}"
    }
    fail -- "@R@-run: exception %opt{!S!_arg2}%opt{!S!_arg1}%opt{!S!_arg3}"
}
DEFPOVH @R@-handle-exc--1  [0] %{ SETG= !S!_arg1 aborted }
DEFPOVH @R@-handle-exc--2  [0] %{ SETG= !S!_arg1 aborted }
DEFPOVH @R@-handle-exc--3  [0] %{ SETG= !S!_arg1 'stack overflow' }
DEFPOVH @R@-handle-exc--4  [0] %{ SETG= !S!_arg1 'stack underflow' }
DEFPOVH @R@-handle-exc--5  [0] %{ SETG= !S!_arg1 'return stack overflow' }
DEFPOVH @R@-handle-exc--6  [0] %{ SETG= !S!_arg1 'return stack underflow' }
DEFPOVH @R@-handle-exc--9  [0] %{ SETG= !S!_arg1 'invalid memory address' }
DEFPOVH @R@-handle-exc--10 [0] %{ SETG= !S!_arg1 'division by zero' }
DEFPOVH @R@-handle-exc--11 [0] %{ SETG= !S!_arg1 'result out of range' }
DEFPOVH @R@-handle-exc--12 [0] %{ SETG= !S!_arg1 'argument type mismatch' }
DEFPOVH @R@-handle-exc--13 [0] %{ SETG= !S!_arg1 'undefined word' }
DEFPOVH @R@-handle-exc--14 [0] %{ SETG= !S!_arg1 'interpreting a compile-only word' }
DEFPOVH @R@-handle-exc--20 [0] %{ SETG= !S!_arg1 'write to a read-only location' }
DEFPOVH @R@-handle-exc--21 [0] %{ SETG= !S!_arg1 'unsupported operation' }
DEFPOVH @R@-handle-exc--22 [0] %{ SETG= !S!_arg1 'control structure mismatch' }
DEFPOVH @R@-handle-exc--24 [0] %{ SETG= !S!_arg1 'invalid numeric argument' }
DEFPOVH @R@-handle-exc--25 [0] %{ SETG= !S!_arg1 'return stack imbalance' }
DEFPOVH @R@-handle-exc--26 [0] %{ SETG= !S!_arg1 'loop parameters unavailable' }
DEFPOVH @R@-handle-exc--27 [0] %{ SETG= !S!_arg1 'invalid recursion' }
DEFPOVH @R@-handle-exc--28 [0] %{ SETG= !S!_arg1 'user interrupt' }
DEFPOVH @R@-handle-exc--29 [0] %{ SETG= !S!_arg1 'compiler nesting' }
DEFPOVH @R@-handle-exc--30 [0] %{ SETG= !S!_arg1 'obsolescent feature' }
DEFPOVH @R@-handle-exc--31 [0] %{ SETG= !S!_arg1 '>BODY used on non-CREATEd definition' }
DEFPOVH @R@-handle-exc--32 [0] %{ SETG= !S!_arg1 'invalid name argument' }
DEFPOVH @R@-handle-exc--36 [0] %{ SETG= !S!_arg1 'invalid file position' }
DEFPOVH @R@-handle-exc--37 [0] %{ SETG= !S!_arg1 'file I/O exception' }
DEFPOVH @R@-handle-exc--38 [0] %{ SETG= !S!_arg1 'non-existent file' }
DEFPOVH @R@-handle-exc--39 [0] %{ SETG= !S!_arg1 'unexpected end of file / missing word' }
DEFPOVH @R@-handle-exc--48 [0] %{ SETG= !S!_arg1 'invalid POSTPONE' }
DEFPOVH @R@-handle-exc--49 [0] %{ SETG= !S!_arg1 'search-order overflow' }
DEFPOVH @R@-handle-exc--50 [0] %{ SETG= !S!_arg1 'search-order underflow' }
DEFPOVH @R@-handle-exc--51 [0] %{ SETG= !S!_arg1 'compilation word list changed' }
DEFPOVH @R@-handle-exc--57 [0] %{ SETG= !S!_arg1 'exception sending or receiving a character' }

DEFP @R@-eval[..] %{
  @R@-clear-internal
  @R@-run %arg{@}
  @R@_STATE-if %{  # compile
    @R@-process-input-failure %{@R@-eval: unterminated definition}
  } %{}
}
decl -hidden str-list @R@_parsed_forth
DEFP @R@-eval-forth[1] %{
  try %{ !S!-streq0 %arg{1} } catch %{
    stkmach-prog-forth2kak @R@_parsed_forth %arg{1}
    eval -save-regs c %{
      reg c %opt{@R@_config_case}
      try     %{
        @R@-config-case -1
        @R@-eval %opt{@R@_parsed_forth}
        SETG= @R@_parsed_forth
        @R@-config-case %reg{c}
      } catch %{
        @R@-config-case %reg{c}
        fail %val{error}
      }
    }
  }
}
DEFP @R@-eval-forth-file[1] %{
  eval "@R@-eval-forth %%file""%arg{1}"""
} -file-completion

decl -hidden str-list @R@_step_action
decl -hidden str @R@_step_parsed1
decl -hidden str @R@_step_xt
DEFP @R@-step[0] %{
  %opt{@R@_config_debug_step}

  try     %{ SLIST-POP-CHK-1_1 @R@_input @R@_step_parsed
  } catch %{ fail @R@-input-stop }
  @R@-word-case-hook %opt{@R@_step_parsed1} @R@_step_parsed1
  try     %{ "@R@-impl-%opt{@R@_step_parsed1}" xt-get @R@_step_xt
    SETG= @R@_step_action "@R@-xtrun-%opt{@R@_step_xt}"
  } catch %{
    SETG= @R@_step_action @R@-step-noncall %opt{@R@_step_parsed1}
  }
  %opt{@R@_step_action}
}
DEFPOVH @R@-step-noncall[1] %{
  try     %{ @R@-number-is %arg{1}
    @R@-stack-PUSH1 %opt{!S!_int}
  } catch %{
    @R@-helper-throw -13 %arg{1}
  }
}
DEFPOVH @R@-number-is[1] %{ @R@-number-is-b10 %arg{1} }  # bootstrap; will redefine
DEFPOVH @R@-number-is-b10[1] %{
  !S!-int-is %arg{1}
}
DEFPOVH @R@-number-is-b16[1] %{
  SETG= !S!_int %sh{ perl -e '$ARGV[0] =~ /^(-?)([0-9a-f]+)$/i or die; print $1 . hex($2)' -- "$1" 2>/dev/null }
}

DEFPOVH @R@-xtcode-0[..] -docstring %{
  XT 0: invalid memory address
} %{ @R@-helper-throw -9 }

decl -hidden str-list @R@_arg_seq_slist
decl -hidden bool @R@_stkimpl_track_cnt false
DEFPOVH @R@-stkimpl-def[4..6] -docstring %{
  Implement a Forth primitive, adding stack-handling boilerplate
  Args: NAME NARGS POPS? BODY [ XT [DRIVER] ]
  Out:
  - @R@_xt (XT integer)
} %{
  try     %{ "!S!-str-literal--%arg{5}"
    @R@_xt-gensym-into @R@_xt
  } catch %{
    SETG= @R@_xt %arg{5}
  }
  SETG= !S!_code_str %{}
  try     %{ "!S!-str-literal-0-%arg{2}"
  } catch %{
    @R@-stkimpl-aux-prelude %arg{@}
  }
  # bind prelude + BODY to XT
  def -params .. -override -hidden -docstring "XT for: %arg{1} [%arg{2}]" "@R@-xtcode-%opt{@R@_xt}" "%opt{!S!_code_str}%arg{4}"
  try     %{ "!S!-str-literal--%arg{6}"
    def -params .. -override -hidden "@R@-xtrun-%opt{@R@_xt}" "@R@-xtcode-%opt{@R@_xt} %%opt{@R@_stack}"
  } catch %{
    def -params .. -override -hidden "@R@-xtrun-%opt{@R@_xt}" "%arg{6} %opt{@R@_xt}"
  }
  decl -hidden str "@R@_xtprop_%opt{@R@_xt}_driver" %arg{6}
  !S!-def-cmd-or-alias "@R@-impl-%arg{1}" """@R@-stkimpl-handle-%%arg{1}"" %opt{@R@_xt} %%arg{@}" %{-params .. -override -hidden}
  try %{ "!S!-str-literal-false-%opt{@R@_stkimpl_track_cnt}"
    SETG- @R@_words %arg{1}
    SETG+ @R@_words %arg{1}
  } catch %{
    !S!-optlen-adjoin @R@_words @R@_words_cnt %arg{1}
  }
}
DEFPOVH @R@-stkimpl-aux-prelude[..] -docstring %{
  Args: like stkimpl-def
} %{
  # - check for underflow
  SETG= !S!_code_str "try %%{!S!-nop-%arg{2}_ %%arg{@}} catch %%{@R@-helper-throw -4 %arg{1}};"
  # - maybe pop (POPS?) args
  try %{ "!S!-str-literal-true-%arg{3}"
    eval "SETG= @R@_arg_seq_slist %%opt{!S!_arg_seq_%arg{2}}"
    SETG+ !S!_code_str "@R@-stack-POPV %opt{@R@_arg_seq_slist};"
  }
}

DEFPOVH @R@-stkimpl-handle-[1..] -docstring %{
  Empty / null command: execute XT
} %{
  "@R@-xtcode-%arg{1}" %opt{@R@_stack}
}
DEFPOVH @R@-stkimpl-handle-run[1..] -docstring %{
  Execute XT, maybe under inner interpreter
} %{
  "@R@-xtrun-%arg{1}"
}
DEFPOVH @R@-stkimpl-handle-nop[..] %{}
DEFPOVH @R@-stkimpl-handle-xt-get[3..] %{
  SETG= %arg{3} %arg{1}
}
DEFPOVH @R@-stkimpl-handle-xt-push[1..] %{
  @R@-stack-PUSH1 %arg{1}
}

DEFPOVH @R@-set-immediate-xt[1] %{
  decl -hidden str "@R@_xtprop_%arg{1}_immediate" ''
}
DEFPOVH @R@-set-immediate[1] %{
  "@R@-impl-%arg{1}" xt-get @R@_xt
  @R@-set-immediate-xt %opt{@R@_xt}
}
DEFPOVH @R@-get-immediate[1] %{
  "@R@-impl-%arg{1}" xt-get @R@_xt
  eval "nop -- %%opt{@R@_xtprop_%opt{@R@_xt}_immediate}"
}

# define STATE + API; interpreter not yet available
decl -hidden str @R@_mem_0 0
@R@-stkimpl-def state 0 false %{
  @R@-stack-PUSH1 0
}
def -override -hidden @R@_STATE-set -params 1 %{
  SETG= @R@_mem_0 %arg{1}
  !S!-forth-if %arg{1} %{  # compile
    SETG= @R@_process_input @R@-compile1
  }                    %{  # interpret
    SETG= @R@_process_input @R@-step
  }
}
def -override -hidden @R@_STATE-if-decide -params 2 %{
  !S!-forth-if-decide %opt{@R@_mem_0} %arg{1} %arg{2}
}
def -override -hidden @R@_STATE-if -params 2 %{
  !S!-forth-if        %opt{@R@_mem_0} %arg{1} %arg{2}
}
SETG= @R@_mem_gensym_cnt 10

decl -hidden str      @R@_compiled_code
!S!-err-def @R@-compile-err
!S!-str-def-literal recurse
DEFPOVH @R@-compile-close[0] -docstring %{
  @R@_stack: POP_XT? NAME XT DRIVER
} %{
  try %{
    SLIST-POP-CHK-1_4 @R@_dstk @R@_stk_arg
  } catch %{
    fail '@R@-compile: unstructured'
  }
  try     %{ "!S!-str-literal--%opt{@R@_stk_arg2}"
    def -override -hidden -params .. -- "@R@-xtcode-%opt{@R@_stk_arg3}" %opt{@R@_compiled_code}
  } catch %{
    @R@-stkimpl-def %opt{@R@_stk_arg2} 0 false %opt{@R@_compiled_code} %opt{@R@_stk_arg3} %opt{@R@_stk_arg4}
    %opt{@R@_msg5} "compiled: %opt{@R@_stk_arg2} = @R@-xtcode-%opt{@R@_stk_arg3}"
  }
  %opt{@R@_msg7} "compiled: def @R@-xtcode-%opt{@R@_stk_arg3} %%{ %opt{@R@_compiled_code} }"
  @R@-forth-if %opt{@R@_stk_arg1} %{
    # popped
  } %{
    @R@-stack-PUSH1 %opt{@R@_stk_arg3}
  }
}
DEFPOVH @R@-compile-start[2..4] -docstring %{
  Args: POP_XT? NAME [ XT [DRIVER] ]
  Out: @R@_xt
} %{
  @R@_STATE-set -1
  SETG= @R@_defproc_name %arg{2}
  try     %{ "!S!-str-literal--%arg{3}"
    @R@_xt-gensym-into @R@_xt
  } catch %{
    SETG= @R@_xt %arg{3}
  }
  @R@-dstk-PUSH %arg{1} %arg{2} %opt{@R@_xt} %arg{4}
  SETG= @R@_compiled_code ''
}
decl -hidden str @R@_cstk_new_xt
DEFPOVH @R@-compile-new-cstk[0] %{
  @R@_xt-gensym-into @R@_cstk_new_xt
  @R@-cstk-PUSH1 %opt{@R@_cstk_new_xt}
}

decl -hidden str-list @R@_compile1_action
DEFPOVH @R@-compile-append[..] %{
  SETG+ @R@_compiled_code "%arg{@};"
}
DEFPOVH @R@-compile-append-quoted[..] %{
  @R@-quote %arg{@}
  SETG+ @R@_compiled_code "%opt{@R@_r4_quote};"
}
DEFPOVH @R@-compile-append-push[..] %{
  SETG+ @R@_compiled_code "@R@-stack-push %arg{@};"
}
DEFPOVH @R@-compile-append-push-quoted[1] %{
  @R@-quote %arg{1}
  SETG+ @R@_compiled_code "@R@-stack-push %opt{@R@_r4_quote};"
}
DEFPOVH @R@-compile-append-xtcall[1] %{
  @R@-compile-append "@R@-xtcode-%arg{1} %%opt{@R@_stack}"
}
DEFPOVH @R@-compile-append-call[1] %{
  try     %{ "@R@-impl-%arg{1}" xt-get @R@_xt
  } catch %{
    @R@-helper-throw -13 %arg{1}
  }
  @R@-compile-append-xtcall %opt{@R@_xt}
}

decl -hidden str @R@_compile_xt
DEFPOVH @R@-compile1[0] %{
  try     %{ SLIST-POP-CHK-1_1 @R@_input @R@_step_parsed
  } catch %{ fail @R@-input-stop }
  @R@-word-case-hook %opt{@R@_step_parsed1} @R@_step_parsed1
  try     %{ "@R@-impl-%opt{@R@_step_parsed1}" xt-get @R@_compile_xt
    SETG= @R@_compile1_action @R@-compile1-call %opt{@R@_compile_xt}
  } catch %{  # no XT
    SETG= @R@_compile1_action @R@-compile1-noncall %opt{@R@_step_parsed1}
  }
  %opt{@R@_compile1_action}
}
DEFPOVH @R@-compile1-call[1] %{
  try     %{ eval "nop -- %%opt{@R@_xtprop_%arg{1}_immediate}"
    SETG= @R@_compile1_action "@R@-xtrun-%arg{1}"
  } catch %{
    SETG= @R@_compile1_action \
    try     %{ eval " ""!S!-str-literal--%%opt{@R@_xtprop_%arg{1}_driver}"" "
      @R@-compile-append-xtcall %arg{1}
    } catch %{
      @R@-compile1-driver-call %arg{1}
    }
  }
  %opt{@R@_compile1_action}
}
decl -hidden str @R@_ret_xt
DEFPOVH @R@-compile1-driver-call[1] %{
  @R@_xt-gensym-into @R@_ret_xt
  @R@-compile-append "@R@-driver1-call %arg{1} %opt{@R@_ret_xt}"
  @R@-compile-close
  @R@-compile-start -1 '' %opt{@R@_ret_xt}
}
DEFPOVH @R@-compile1-noncall[1] %{
  try     %{ @R@-number-is %arg{1}
    @R@-compile-append-push %opt{!S!_int}
  } catch %{
    @R@-helper-throw -13 %arg{1}
  }
}

!S!-err-def @R@-driver1-jump-exc
!S!-err-def @R@-driver1-stop
DEFPOVH @R@-driver1[1] %{
  @R@-rstk-PUSH %arg{1} -1
  try %{
    !S!-loop-inf eval %{
      try     %{ SLIST-POP-CHK-1_1 @R@_rstk @R@_stk_arg
      } catch %{
        @R@-helper-throw -25
      }
      try %{ INTGT0 %opt{@R@_stk_arg1}
      } catch %{
        fail @R@-driver1-stop
      }
      try %{
        "@R@-xtcode-%opt{@R@_stk_arg1}" %opt{@R@_stack}
      } catch %{ !S!-err-eq @R@-driver1-jump-exc
      }
    }
  } catch %{ !S!-err-eq @R@-driver1-stop }
}
DEFPOVH @R@-driver1-jump[0] %{
  fail @R@-driver1-jump-exc
}
DEFPOVH @R@-driver1-push-dst[..] %{
  @R@-rstk-PUSH %arg{@}
}
DEFPOVH @R@-driver1-jump-to[1] %{
  @R@-rstk-PUSH %arg{1}
  fail @R@-driver1-jump-exc
}
DEFPOVH @R@-driver1-0jump-to[1] %{
  try %{ SLIST-POP-CHK-1_1 @R@_stack @R@_stk_arg } catch %{
    @R@-helper-throw -4
  }
  try     %{ "!S!-str-literal-0-%opt{@R@_stk_arg1}"
    @R@-rstk-PUSH %arg{1}
    SETG= @R@_action_slist fail @R@-driver1-jump-exc
  } catch %{
    SETG= @R@_action_slist
  }
  %opt{@R@_action_slist}
}
DEFPOVH @R@-driver1-call[2] -docstring %{
  Args: CALL_XT RETURN_XT
} %{
  @R@-rstk-PUSH %arg{@}
  fail @R@-driver1-jump-exc
}

DEFPOVH @R@_STATE-assert-compile[1] %{
  decl -hidden str @R@_STATE_assert_source %arg{1}  # -if doesn't do %arg{}
  @R@_STATE-if %{  # compile
  } %{             # interpret
    @R@-helper-throw -14 %opt{@R@_STATE_assert_source}
  }
}


# Forth primitive implementations

@R@-stkimpl-def quit 0 false %{
  @R@-clear-internal
  @R@-input-set
  fail @R@-input-stop
}
@R@-stkimpl-def abort 0 false %{
  @R@-stack-PUSH1 -1
  @R@-impl-throw
}
@R@-stkimpl-def '\\kak\str-abort"' 2 true %{
  decl -hidden str @R@_str_abort_msg %arg{1}
  SETG= @R@_action_slist @R@-helper-throw -2 %arg{1}
  @R@-forth-if %arg{2} %{} %{
    SETG= @R@_action_slist
  }
  %opt{@R@_action_slist}
}
!S!-err-def @R@-throw
@R@-stkimpl-def throw 1 true %{
  try %{ !S!-inteq0 %arg{1}
    # discard
  } catch %{
    @R@-helper-throw %arg{1}
  }
}
DEFPOVH @R@-helper-throw[1..2] %{
  fail "@R@-throw %arg{1} %%%arg{2}"
}

@R@-stkimpl-def parse-name 0 false %{
  try %{ SLIST-POP-CHK-1_1 @R@_input @R@_input_arg } catch %{
    @R@-helper-throw -39 parse-name
  }
  @R@-stack-PUSH1 %opt{@R@_input_arg1}
  @R@-impl-\\kak\str-s"  # TODO: s-quote is state smart, does it matter?
}

@R@-stkimpl-def exit 0 false %{
  @R@_STATE-assert-compile exit
  @R@-compile-append @R@-driver1-jump
}
@R@-set-immediate-xt %opt{@R@_xt}

@R@-stkimpl-def ahead 0 false %{
  @R@_STATE-assert-compile ahead
  @R@-compile-new-cstk
  @R@-compile-append "@R@-driver1-jump-to %opt{@R@_cstk_new_xt}"
}
@R@-set-immediate-xt %opt{@R@_xt}

decl -hidden str @R@_then_xt1
decl -hidden str @R@_else_xt1
decl -hidden str @R@_begin_xt1
decl -hidden str @R@_while_xt1

@R@-stkimpl-def begin 0 false %{
  @R@_STATE-assert-compile begin
  @R@-compile-new-cstk
  SETG= @R@_begin_xt1 %opt{@R@_cstk_new_xt}
  @R@-compile-append "@R@-driver1-jump-to %opt{@R@_begin_xt1}"
  @R@-compile-close
  @R@-compile-start -1 '' %opt{@R@_begin_xt1}
}
@R@-set-immediate-xt %opt{@R@_xt}

@R@-stkimpl-def again 0 false %{
  @R@_STATE-assert-compile again
  try %{ SLIST-POP-CHK-1_1 @R@_cstk @R@_begin_xt } catch %{
    @R@-helper-throw -22 again
  }
  @R@-compile-append "@R@-driver1-jump-to %opt{@R@_begin_xt1}"
}
@R@-set-immediate-xt %opt{@R@_xt}

@R@-stkimpl-def then 0 false %{
  @R@_STATE-assert-compile then
  try %{ SLIST-POP-CHK-1_1 @R@_cstk @R@_then_xt } catch %{
    @R@-helper-throw -22 then
  }
  @R@-compile-append "@R@-driver1-jump-to %opt{@R@_then_xt1}"
  @R@-compile-close
  @R@-compile-start -1 '' %opt{@R@_then_xt1}
}
@R@-set-immediate-xt %opt{@R@_xt}

@R@-stkimpl-def if 0 false %{
  @R@_STATE-assert-compile if
  @R@-compile-new-cstk
  @R@-compile-append "@R@-driver1-0jump-to %opt{@R@_cstk_new_xt}"
}
@R@-set-immediate-xt %opt{@R@_xt}

@R@-stkimpl-def else 0 false %{
  @R@_STATE-assert-compile else
  try %{ SLIST-POP-CHK-1_1 @R@_cstk @R@_else_xt } catch %{
    @R@-helper-throw -22 else
  }
  @R@-impl-ahead
  @R@-cstk-PUSH %opt{@R@_else_xt1}
  @R@-impl-then
}
@R@-set-immediate-xt %opt{@R@_xt}

@R@-stkimpl-def while 0 false %{
  @R@_STATE-assert-compile while
  try %{ SLIST-POP-CHK-1_1 @R@_cstk @R@_while_xt } catch %{
    @R@-helper-throw -22 while
  }
  @R@-impl-if
  @R@-cstk-PUSH %opt{@R@_while_xt1}
}
@R@-set-immediate-xt %opt{@R@_xt}

@R@-stkimpl-def until 0 false %{
  @R@_STATE-assert-compile until
  @R@-compile-append-call 0=
  try %{ SLIST-POP-CHK-1_1 @R@_cstk @R@_while_xt } catch %{
    @R@-helper-throw -22 while
  }
  @R@-impl-if
  @R@-cstk-PUSH %opt{@R@_while_xt1}
  @R@-impl-again
  @R@-impl-then
}
@R@-set-immediate-xt %opt{@R@_xt}

@R@-stkimpl-def repeat 0 false %{
  @R@_STATE-assert-compile repeat
  @R@-impl-again
  @R@-impl-then
}
@R@-set-immediate-xt %opt{@R@_xt}

@R@-stkimpl-def case 0 false %{
  @R@_STATE-assert-compile case
  @R@-compile-new-cstk
}
@R@-set-immediate-xt %opt{@R@_xt}
@R@-stkimpl-def endcase 0 false %{
  @R@_STATE-assert-compile endcase
  @R@-impl-then
  @R@-compile-append-call drop
}
@R@-set-immediate-xt %opt{@R@_xt}

@R@-stkimpl-def of 0 false %{
  @R@_STATE-assert-compile of
  @R@-compile-append-call over
  @R@-compile-append-call =
  @R@-impl-if
  @R@-compile-append-call drop
}
@R@-set-immediate-xt %opt{@R@_xt}
@R@-stkimpl-def endof 0 false %{
  @R@_STATE-assert-compile endof
  @R@-compile-append-push -1  # dummy for ENDCASE to drop
  # cstk: IF-XT CASE-XT ...
  try %{ !S!-args-2th-into @R@_xt %opt{@R@_cstk} } catch %{
    @R@-helper-throw -22 endof
  }
  @R@-compile-append "@R@-driver1-jump-to %opt{@R@_xt}"
  @R@-impl-then
}
@R@-set-immediate-xt %opt{@R@_xt}

@R@-stkimpl-def do 0 false %{
  @R@_STATE-assert-compile do
  @R@-compile-new-cstk
  @R@-compile-append "@R@-driver1-push-dst %opt{@R@_cstk_new_xt}"  # breaks EXIT; but EXIT within DO..LOOP needs UNLOOP
  @R@-compile-append %{@R@-runtime-do %opt{@R@_stack}}
  @R@-impl-begin
}
DEFPOVH @R@-runtime-do[..] %{
  try %{ !S!-nop-2_ %arg{@}
    SETG- @R@_stack %arg{1} %arg{2}
  } catch %{
    @R@-helper-throw -4 do
  }
  @R@-lstk-PUSH %arg{1} %arg{2}
}
@R@-set-immediate-xt %opt{@R@_xt}
@R@-stkimpl-def ?do 0 false %{
  @R@_STATE-assert-compile ?do
  @R@-compile-append-call 2dup
  @R@-compile-append-call =
  @R@-impl-if
  @R@-compile-append-call 2drop
  @R@-impl-else
  !S!-args-1th-into @R@_xt %opt{@R@_cstk}
  @R@-compile-append "@R@-driver1-push-dst %opt{@R@_xt}"
  @R@-compile-append %{@R@-runtime-do %opt{@R@_stack}}
  @R@-impl-begin
}
@R@-set-immediate-xt %opt{@R@_xt}
@R@-stkimpl-def unloop 0 false %{
  !S!-slist-del-1_2 @R@_lstk
  !S!-slist-del-1_1 @R@_rstk
}
@R@-stkimpl-def leave 0 false %{
  @R@_STATE-assert-compile leave
  @R@-compile-append %{!S!-slist-del-1_2 @R@_lstk}
  @R@-compile-append %{@R@-driver1-jump}
}
@R@-set-immediate-xt %opt{@R@_xt}

decl -hidden int @R@_doloop_int1  # counter
@R@-stkimpl-def +loop 0 false %{
  @R@_STATE-assert-compile +loop
  @R@-compile-append %{@R@-runtime-doloop-chk1 %opt{@R@_lstk}}
  @R@-impl-if
  @R@-impl-leave
  @R@-impl-then
  @R@-impl-again
  @R@-impl-then
}
@R@-set-immediate-xt %opt{@R@_xt}
@R@-stkimpl-def loop 0 false %{
  @R@_STATE-assert-compile loop
  @R@-compile-append-push 1
  @R@-impl-+loop
}
@R@-set-immediate-xt %opt{@R@_xt}
@R@-stkimpl-def i 0 false %{
  @R@_STATE-assert-compile i
  @R@-compile-append %{@R@-runtime-ijk 2 %opt{@R@_lstk}}
}
@R@-set-immediate-xt %opt{@R@_xt}
@R@-stkimpl-def j 0 false %{
  @R@_STATE-assert-compile j
  @R@-compile-append %{@R@-runtime-ijk 4 %opt{@R@_lstk}}
}
@R@-set-immediate-xt %opt{@R@_xt}
@R@-stkimpl-def k 0 false %{
  @R@_STATE-assert-compile k
  @R@-compile-append %{@R@-runtime-ijk 6 %opt{@R@_lstk}}
}
@R@-set-immediate-xt %opt{@R@_xt}
DEFPOVH @R@-runtime-ijk[3..] %{
  eval "@R@-stack-push %%arg{%arg{1}}"
}

DEFPOVH @R@-runtime-doloop-chk1[2..] -docstring %{
  Pushes -1 if loop done, 0 to continue
  Args: @R@_lstk = i l ..
} %{
  try %{ SLIST-POP-CHK-1_1 @R@_stack @R@_doloop_int } catch %{
    @R@-helper-throw -4 +loop
  }
  SETG- @R@_lstk %arg{1}
  @R@-runtime-doloop-chk2 %arg{1} %arg{2} %opt{@R@_doloop_int1}
}
DEFPOVH @R@-runtime-doloop-chk2[3] -docstring %{
  Args: i l d
} %{
  SETG+ @R@_doloop_int1 %arg{1}
  @R@-lstk-PUSH %opt{@R@_doloop_int1}

  # detect overflow when adding 'd' to (i - l + int_min)
  SETG- @R@_doloop_int1 %arg{2}
  SETG+ @R@_doloop_int1 %opt{!S!_int32_min}  # = i - l + int_min + d
  try     %{ INTGT0 %opt{@R@_doloop_int1}
    try     %{ INTGT0 %arg{3}
      @R@-stack-PUSH1  0
    } catch %{
      SETG- @R@_doloop_int1 %arg{3}
      try %{ INTGT0 %opt{@R@_doloop_int1}
        @R@-stack-PUSH1  0
      } catch %{
        @R@-stack-PUSH1 -1
      }
    }
  } catch %{ "!S!-str-literal-0-%opt{@R@_doloop_int1}"
    try     %{
      "!S!-str-literal-%opt{!S!_int32_min}-%arg{3}"
      @R@-stack-PUSH1 -1
    } catch %{
      @R@-stack-PUSH1  0
    }
  } catch %{
    try     %{
      INTGT0 %arg{3}
      SETG- @R@_doloop_int1 %arg{3}
      INTGT0 %opt{@R@_doloop_int1}
      @R@-stack-PUSH1 -1
    } catch %{
      @R@-stack-PUSH1  0
    }
  }
}

@R@-stkimpl-def words 0 false %{
  @R@-print "%opt{@R@_words}
"
}

@R@-stkimpl-def immediate 0 false %{
  @R@-set-immediate-xt %opt{@R@_defproc_xt}
}

@R@-stkimpl-def compile, 1 true %{
  @R@-compile-append-xtcall %arg{1}
}

@R@-stkimpl-def postpone 0 false %{
  try %{ SLIST-POP-CHK-1_1 @R@_input @R@_input_arg } catch %{
    @R@-helper-throw -39 postpone
  }
  @R@-word-case-hook %opt{@R@_input_arg1} @R@_input_arg1
  "@R@-impl-%opt{@R@_input_arg1}" xt-get @R@_xt
  try     %{ eval "nop -- %%opt{@R@_xtprop_%opt{@R@_xt}_immediate}"
    @R@-compile-append-xtcall %opt{@R@_xt}
  } catch %{
    @R@-compile-append "@R@-compile-append-xtcall %opt{@R@_xt}"
  }
}
@R@-set-immediate-xt %opt{@R@_xt}

@R@-stkimpl-def recurse 0 false %{
  @R@-compile1-driver-call %opt{@R@_defproc_xt}
}
@R@-set-immediate-xt %opt{@R@_xt}

@R@-stkimpl-def  literal 1 true %{
  @R@-compile-append-push %arg{1}
}
@R@-stkimpl-def 2literal 2 true %{
  @R@-compile-append-push %arg{2}
  @R@-compile-append-push %arg{1}
}
@R@-stkimpl-def sliteral 2 true %{
  @R@-compile-append-push %arg{2}
  @R@-compile-append-push %arg{1}
}
@R@-stkimpl-def \\kak\qliteral 1 true %{
  @R@-compile-append-push-quoted %arg{1}
}
@R@-set-immediate  literal
@R@-set-immediate 2literal
@R@-set-immediate sliteral
@R@-set-immediate \\kak\qliteral

@R@-stkimpl-def [ 0 false %{
  @R@_STATE-set 0
}
@R@-set-immediate-xt %opt{@R@_xt}
@R@-stkimpl-def ] 0 false %{
  @R@_STATE-set -1
}

@R@-stkimpl-def include  0 false %{
  try %{ SLIST-POP-CHK-1_1 @R@_input @R@_input_arg
  } catch %{ @R@-helper-throw -39 include }
  @R@-helper-include %opt{@R@_input_arg1}
}
@R@-stkimpl-def included 2 true %{
  @R@-helper-forth2str %arg{2} %arg{1}
  @R@-helper-include %opt{@R@_r4_forth2str}
}
DEFPOVH @R@-helper-include[1] %{
  try     %{ eval "SETG= !S!_str %%file{%arg{1}}"
  } catch %{
    @R@-helper-throw -38 %arg{1}
  }
  eval -save-regs i %{
    reg i guard %opt{@R@_input}
    @R@-eval-forth %opt{!S!_str}
    SETG= @R@_input %reg{i}; SETG- @R@_input guard
  }
}

}
eval "set -add global stkmach_boot_code %%file""%opt{stkmach_source}.defs"""
eval "set -add global stkmach_boot_code %%file""%opt{stkmach_source}.int.defs"""
eval "set -add global stkmach_boot_code %%file""%opt{stkmach_source}.mem.defs"""
set -add global stkmach_boot_code %{

# ':' ... ';' (define words)

decl -hidden str      @R@_defproc_name
decl -hidden str      @R@_defproc_xt
decl -hidden str-list @R@_defproc_code
decl -hidden str-list @R@_defproc_epilog
!S!-err-def @R@-defproc-stop

@R@-stkimpl-def : 0 false %{
  try %{ SLIST-POP-CHK-1_1 @R@_input @R@_input_arg } catch %{
    @R@-helper-throw -39 :
  }
  @R@-word-case-hook %opt{@R@_input_arg1} @R@_input_arg1
  @R@-compile-start -1 %opt{@R@_input_arg1} '' @R@-driver1
  SETG= @R@_defproc_xt %opt{@R@_xt}
}
@R@-stkimpl-def :noname 0 false %{
  @R@-compile-start 0 '' '' @R@-driver1
  SETG= @R@_defproc_xt %opt{@R@_xt}
}
@R@-stkimpl-def ';' 0 false %{
  @R@_STATE-assert-compile ';'
  try     %{ !S!-nop-0_0 %opt{@R@_cstk}
  } catch %{
    fail '@R@-compile: unstructured'
  }
  @R@-compile-close
  @R@_STATE-set  0
}
@R@-set-immediate-xt %opt{@R@_xt}
@R@-stkimpl-def \\kak\:macro 0 false %{
  try %{ SLIST-POP-CHK-1_1 @R@_input @R@_input_parsed_ } catch %{
    @R@-helper-throw -39 :
  }
  @R@-word-case-hook %opt{@R@_input_parsed_1} @R@_input_parsed_1
  SETG= @R@_defproc_name %opt{@R@_input_parsed_1}
  @R@-helper-defproc-parse
  @R@-helper-defproc-bind %opt{@R@_defproc_name} %opt{@R@_defproc_code}
}
DEFPOVH @R@-helper-defproc-parse[0] %{
  SETG= @R@_defproc_code
  SETG= @R@_defproc_epilog
  try %{
    !S!-loop-inf @R@-helper-defproc-parse1
  } catch %{ !S!-err-eq @R@-defproc-stop
    SETG+ @R@_defproc_code %opt{@R@_defproc_epilog}
  }
}
DEFPOVH @R@-helper-defproc-parse1[0] %{
  try %{ @R@-input-parse1 } catch %{
    fail %{@R@-run: unterminated definition}
  }
  try %{ "!S!-str-literal-recurse-%opt{@R@_input_parsed_1}"
    # TODO: no RECURSE outside ':', or after 'DOES>'
    @R@-input-PUSH %opt{@R@_defproc_name}
  } catch %{ !S!-streq %opt{@R@_input_parsed_1} 'does>'
    SETG+ @R@_defproc_code does>
    SETG+ @R@_defproc_epilog ';'
  } catch %{ !S!-strne %opt{@R@_input_parsed_1} ';'
    SETG+ @R@_defproc_code %opt{@R@_input_parsed_1} %opt{@R@_input_parsed_2o}
  } catch %{
    fail @R@-defproc-stop
  }
}
DEFPOVH @R@-udf-getopt[2] %{
  "@R@-impl-%arg{1}" xt-get @R@_xt
  SETG= %arg{2} "@R@_xtprop_%opt{@R@_xt}_coloncode"
}
DEFPOVH @R@-helper-defproc-bind[..] -docstring %{
  Args: WORD OP..
} %{
  @R@_xt-gensym-into @R@_defproc_xt
  decl -hidden str @R@_defproc_gsym "@R@_xtprop_%opt{@R@_defproc_xt}_coloncode"
  decl -hidden str-list %opt{@R@_defproc_gsym}
  SETG= %opt{@R@_defproc_gsym} %arg{@}
  SETG- %opt{@R@_defproc_gsym} %arg{1}
  @R@-stkimpl-def %arg{1} 0 false "@R@-input-push %%opt{%opt{@R@_defproc_gsym}}" %opt{@R@_defproc_xt}
}

decl -hidden str @R@_varname1  # last created word
decl -hidden int @R@_varaddr1  # last created address

@R@-stkimpl-def constant 1 true %{
  try %{ SLIST-POP-CHK-1_1 @R@_input @R@_varname } catch %{
    @R@-helper-throw -39 constant/value
  }
  @R@-word-case-hook %opt{@R@_varname1} @R@_varname1
  @R@-helper-constant %opt{@R@_varname1} %arg{1}
}
@R@-stkimpl-def value  0 false %{
  @R@-impl-create
  def -override -hidden -params .. -- "@R@-xtcode-%opt{@R@_xt}" "
    @R@-stack-push %opt{@R@_varaddr1}
    @R@-impl-@
  "
  def -override -hidden -params .. -- "@R@-xtcode_to-%opt{@R@_xt}" "
    @R@-stack-push %opt{@R@_varaddr1}
    @R@-impl-!
  "
  @R@-impl-,
}
@R@-stkimpl-def 2value 0 false %{
  @R@-impl-create
  def -override -hidden -params .. -- "@R@-xtcode-%opt{@R@_xt}" "
    @R@-stack-push %opt{@R@_varaddr1}
    @R@-impl-2@
  "
  def -override -hidden -params .. -- "@R@-xtcode_to-%opt{@R@_xt}" "
    @R@-stack-push %opt{@R@_varaddr1}
    @R@-impl-2!
  "
  @R@-impl-2,
}
@R@-stkimpl-def to     0 false %{
  try %{ SLIST-POP-CHK-1_1 @R@_input @R@_input_arg } catch %{
    @R@-helper-throw -39 to
  }
  @R@-word-case-hook %opt{@R@_input_arg1} @R@_input_arg1
  try     %{
    "@R@-impl-%opt{@R@_input_arg1}" xt-get @R@_xt
  } catch %{
    @R@-helper-throw -13 %opt{@R@_input_arg1}
  }
  SETG= @R@_action_slist
  @R@_STATE-if %{  # compile
    @R@-compile-append "@R@-xtcode_to-%opt{@R@_xt} %%opt{@R@_stack}"
  }            %{  # interpret
    "@R@-xtcode_to-%opt{@R@_xt}" %opt{@R@_stack}
  }
}
@R@-set-immediate-xt %opt{@R@_xt}
DEFPOVH @R@-helper-constant[2] %{
  try %{ !S!-int-is %arg{2}
    @R@-stkimpl-def %arg{1} 0 false "@R@-stack-push %opt{!S!_int}"
  } catch %{
    @R@-quote %arg{2}
    @R@-stkimpl-def %arg{1} 0 false "@R@-stack-push %opt{@R@_r4_quote}"
  }
  SETG= @R@_defproc_xt %opt{@R@_xt}
  SETG= @R@_varaddr1 %opt{!S!_int32_min}
}

@R@-stkimpl-def create 0 false %{
  try %{ SLIST-POP-CHK-1_1 @R@_input @R@_varname } catch %{
    @R@-helper-throw -39 create
  }
  @R@-word-case-hook %opt{@R@_varname1} @R@_varname1
  SETG= @R@_varaddr1 %opt{@R@_mem_gensym_cnt}
  @R@-stkimpl-def %opt{@R@_varname1} 0 false "SETG= @R@_stack %opt{@R@_varaddr1} %%opt{@R@_stack}"
  SETG= @R@_defproc_xt %opt{@R@_xt}
  decl -hidden int "@R@_xtprop_%opt{@R@_xt}_body" %opt{@R@_varaddr1}
}
@R@-stkimpl-def >body 1 true %{
  try     %{
    eval "@R@-stack-push %%opt{@R@_xtprop_%arg{1}_body}"
  } catch %{
    @R@-helper-throw -31
  }
}

decl -hidden str @R@_does_xt
@R@-stkimpl-def does> 0 false %{
  @R@_STATE-if %{  # compile
    @R@_xt-gensym-into @R@_does_xt
    @R@-compile-append "@R@-compile1-does %opt{@R@_does_xt}"
    @R@-compile-close
    @R@-compile-start -1 '' %opt{@R@_does_xt}
  }            %{  # interpret
    @R@-helper-does
  }
}
@R@-set-immediate-xt %opt{@R@_xt}
DEFPOVH @R@-helper-does[0] %{
  "@R@-impl-%opt{@R@_varname1}" xt-get @R@_defproc_xt
  @R@-compile-start -1 %opt{@R@_varname1} %opt{@R@_defproc_xt} @R@-driver1
  @R@-compile-append-push %opt{@R@_varaddr1}
}
DEFPOVH @R@-compile1-does[1] %{
  @R@-helper-does
  @R@-compile-append-xtcall %arg{1}
  @R@-compile-close
  @R@_STATE-set 0
}

@R@-stkimpl-def variable 0 false %{
  @R@-impl-create
  @R@-mem-allot1 0
}
@R@-stkimpl-def 2variable 0 false %{
  @R@-impl-create
  @R@-mem-allot1 0
  @R@-mem-allot1 0
}
SETG+ @R@_boot_late_code : 2constant create 2, does> 2@ \;

@R@-stkimpl-def find 1 false %{
  @R@-impl-count
  SLIST-POP-CHK-1_2 @R@_stack @R@_stk_arg
  @R@-helper-forth2str %opt{@R@_stk_arg2} %opt{@R@_stk_arg1}
  @R@-word-case-hook %opt{@R@_r4_forth2str} @R@_r4_forth2str
  try     %{ !S!-optlen-member @R@_words %opt{@R@_words_cnt} %opt{@R@_r4_forth2str}
    @R@-stack-PUSH1 "@R@-impl-%opt{@R@_r4_forth2str}"
    try     %{ @R@-get-immediate %opt{@R@_r4_forth2str}
      @R@-stack-PUSH1  1
    } catch %{
      @R@-stack-PUSH1 -1
    }
  } catch %{
    @R@-stack-PUSH 0 %arg{1}
  }
}

@R@-stkimpl-def [defined] 0 false %{
  try %{ SLIST-POP-CHK-1_1 @R@_input @R@_input_arg } catch %{
    @R@-helper-throw -39 [defined]
  }
  @R@-word-case-hook %opt{@R@_input_arg1} @R@_input_arg1
  try     %{ !S!-optlen-member @R@_words %opt{@R@_words_cnt} %opt{@R@_input_arg1}
    @R@-stack-PUSH1 -1
  } catch %{
    @R@-stack-PUSH1 0
  }
}
@R@-set-immediate-xt %opt{@R@_xt}
@R@-stkimpl-def [undefined] 0 false %{
  @R@-impl-[defined]
  @R@-impl-invert
}
@R@-set-immediate-xt %opt{@R@_xt}

@R@-stkimpl-def only  0 false %{}
@R@-stkimpl-def forth 0 false %{}
@R@-stkimpl-def definitions 0 false %{}
@R@-stkimpl-def forth-wordlist 0 false %{
  @R@-stack-PUSH 0
}
@R@-stkimpl-def get-current 0 false %{
  @R@-stack-PUSH 0
}
@R@-stkimpl-def set-current 1 true %{
}
@R@-stkimpl-def order 0 false %{
  @R@-print 'Forth '
}

@R@-stkimpl-def char 0 false %{
  # TODO: speed up [[:alnum:]-_] with lookup def's
  try %{ SLIST-POP-CHK-1_1 @R@_input @R@_input_arg } catch %{
    @R@-helper-throw -39 char
  }
  !S!-1st-char-code %opt{@R@_input_arg1}
  @R@-stack-PUSH1 %opt{!S!_r4_1st_char_code}
}
@R@-stkimpl-def [char] 0 false %{
  try %{ SLIST-POP-CHK-1_1 @R@_input @R@_input_arg } catch %{
    @R@-helper-throw -39 [char]
  }
  !S!-1st-char-code %opt{@R@_input_arg1}
  @R@-compile-append-push %opt{!S!_r4_1st_char_code}
}
@R@-set-immediate-xt %opt{@R@_xt}

@R@-stkimpl-def \' 0 false %{
  try %{ SLIST-POP-CHK-1_1 @R@_input @R@_input_arg } catch %{
    @R@-helper-throw -39 \'
  }
  @R@-word-case-hook %opt{@R@_input_arg1} @R@_input_arg1
  try     %{ "@R@-impl-%opt{@R@_input_arg1}" xt-push
  } catch %{
    @R@-helper-throw -13 %opt{@R@_input_arg1}
  }
}
@R@-stkimpl-def ['] 0 false %{
  @R@_STATE-assert-compile [']
  try %{ SLIST-POP-CHK-1_1 @R@_input @R@_input_arg } catch %{
    @R@-helper-throw -39 [']
  }
  @R@-word-case-hook %opt{@R@_input_arg1} @R@_input_arg1
  try     %{ "@R@-impl-%opt{@R@_input_arg1}" xt-get @R@_xt
  } catch %{
    @R@-helper-throw -13 %opt{@R@_input_arg1}
  }
  @R@-compile-append-push %opt{@R@_xt}
}
@R@-set-immediate-xt %opt{@R@_xt}

# EXECUTE must activate inner interpreter
@R@-stkimpl-def execute 1 true %{
  "@R@-xtcode-%arg{1}" %opt{@R@_stack}
} '' @R@-driver1

SETG+ @R@_boot_late_code : defer create 0 , does> @ execute \;
decl -hidden str @R@_defer_word1
@R@-stkimpl-def is 0 false %{
  try %{ SLIST-POP-CHK-1_1 @R@_input @R@_defer_word } catch %{
    @R@-helper-throw -39 is
  }
  @R@-word-case-hook %opt{@R@_defer_word1} @R@_defer_word1
  try     %{
    "@R@-impl-%opt{@R@_defer_word1}" xt-get @R@_xt
  } catch %{
    @R@-helper-throw -13 %opt{@R@_defer_word1}
  }
  SETG= @R@_action_slist
  @R@_STATE-if %{  # compile
    SETG= @R@_action_slist @R@-compile-append
  } %{}            # interpret
  %opt{@R@_action_slist} @R@-helper-is %opt{@R@_xt}
}
@R@-set-immediate-xt %opt{@R@_xt}
DEFPOVH @R@-helper-is[1] %{
  @R@-stack-PUSH1 %arg{1}
  @R@-impl-defer!
}
@R@-stkimpl-def defer! 0 false %{
  @R@-impl->body
  @R@-impl-!
}
@R@-stkimpl-def defer@ 0 false %{
  @R@-impl->body
  @R@-impl-@
}
@R@-stkimpl-def action-of 0 false %{
  @R@_STATE-if %{  # compile
    @R@-impl-[']
    @R@-compile-append-call defer@
  } %{             # interpret
    @R@-impl-'
    @R@-impl-defer@
  }
}
@R@-set-immediate-xt %opt{@R@_xt}

@R@-stkimpl-def [if] 1 true %{
  @R@-helper-if-parse
  @R@-forth-if %arg{1} %{
    @R@-input-PUSH %opt{@R@_input_parsed_if_then}
  } %{
    @R@-input-PUSH %opt{@R@_input_parsed_if_else}
  }
}
@R@-set-immediate-xt %opt{@R@_xt}

decl -hidden str-list @R@_input_parsed_if_then
decl -hidden str-list @R@_input_parsed_if_else
decl -hidden str @R@_helper_if_parse_part
DEFPOVH @R@-helper-if-parse[0] -docstring %{
  Parse an IF..[ELSE..]..THEN statement from @R@_input
  Results:
  - @R@_input_parsed_if_then
  - @R@_input_parsed_if_else
} %{
  SETG= @R@_depth 1
  SETG= @R@_input_parsed_if_then
  SETG= @R@_input_parsed_if_else
  SETG= @R@_helper_if_parse_part @R@_input_parsed_if_then
  try %{
    !S!-loop-inf @R@-helper-if-parse1
  } catch %{ !S!-err-eq @R@-skip-stop }
}
!S!-batch-def @R@-helper-if-parse-handle- '' %{-override -hidden} \
  [then] %{
    SETG+ @R@_depth -1
    try %{
      !S!-inteq0 %opt{@R@_depth}
      SETG= @R@_action_slist fail @R@-skip-stop
    }
  } \
  [else] %{
    try %{ !S!-inteq1 %opt{@R@_depth}
      # top-level ELSE: skip, switch target slist; TODO: enforce single ELSE
      SETG= @R@_helper_if_parse_part @R@_input_parsed_if_else
      SETG= @R@_action_slist
    }
  } \
  [if] %{
    SETG+ @R@_depth 1
  }
DEFPOVH @R@-helper-if-parse1[0] %{
  try %{ @R@-input-parse1 } catch %{ fail '@R@-run: unterminated [if]' }
  # normally, add parsed word(s) to current target slist
  SETG= @R@_action_slist SETG+ %opt{@R@_helper_if_parse_part} %opt{@R@_input_parsed_1} %opt{@R@_input_parsed_2o}
  @R@-word-case-hook %opt{@R@_input_parsed_1} @R@_input_parsed_1
  try %{ "@R@-helper-if-parse-handle-%opt{@R@_input_parsed_1}" }
  %opt{@R@_action_slist}
}

SETG= @R@_stkimpl_track_cnt true
try %{ !S!-args-length-errret %opt{@R@_words}
} catch %{
  SETG= @R@_words_cnt %val{error}
}

#@R@-config-debug
@R@-eval %opt{@R@_boot_late_code}
# enable other bases; TODO: hardcoded BASE addr for efficiency
DEFPOVH @R@-number-is[1] %{ "@R@-number-is-b%opt{@R@_mem_10}" %arg{1} }
@R@-eval-forth-file "%opt{stkmach_source}.defs.fs"

@R@-clear
@R@-config-verbose 5
}  # end boot code

def stkmach-boot -params 1 %{

try %{
  decl -hidden bool "%arg{1}_boot" true
} catch %{
  fail "stkmach-boot: invalid machine name: |%arg{1}|"
}

decl -hidden str stkmach_boot_loader_pfx_subst "
  exec '%%s' @R@-  <ret>c %arg{1}- <esc>
  exec '%%s' @R@_  <ret>c %arg{1}_ <esc>
  try %%{ exec '%%s' @R@:- <ret>c %arg{1}  <esc> }
  try %%{ exec '%%s' @R@:_ <ret>c %arg{1}  <esc> }
"

stkmach_loader '' '' %opt{stkmach_boot_code} %{
  %opt{stkmach_debug_timer} stkmach_loader_do_fmt

  exec '%s' @R@-(\w+)-POPV   [^\S\n]+          <ret>c 'SETG- @R@_<c-r>1 ' <esc>
  # use on separate line only
  %opt{stkmach_debug_timer} exec '%s' @R@-(\w+)-PUSH1? [^\S\n]+ ([^\n]+) <ret>c 'SETG= @R@_<c-r>1 <c-r>2 %opt{@R@_<c-r>1}' <esc>

  %opt{stkmach_debug_timer} eval %opt{stkmach_boot_loader_pfx_subst}

  %opt{stkmach_debug_timer} stkmach_loader_do_macros
  %opt{stkmach_debug_timer} stkmach_loader_do_kks_pfx
}

} -override  # end boot

}  # end module stkmach-instance
