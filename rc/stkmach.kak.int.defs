@R@-stkimpl-def + 2 true %{
  INT-ADDSUB -add    %arg{2} %arg{1}
  @R@-stack-PUSH1 %opt{!S!_int}
}
@R@-stkimpl-def - 2 true %{
  INT-ADDSUB -remove %arg{2} %arg{1}
  @R@-stack-PUSH1 %opt{!S!_int}
}
@R@-stkimpl-def 1+ 1 true %{
  INT-ADDSUB -add  1 %arg{1}
  @R@-stack-PUSH1 %opt{!S!_int}
}
@R@-stkimpl-def 1- 1 true %{
  INT-ADDSUB -add -1 %arg{1}
  @R@-stack-PUSH1 %opt{!S!_int}
}
@R@-stkimpl-def * 2 true %{
  !S!-int-mul %arg{1} %arg{2}
  @R@-stack-PUSH1 %opt{!S!_r4_mul}
}
@R@-stkimpl-def / 2 true %{
  !S!-int-div %arg{2} %arg{1}
  @R@-stack-PUSH1 %opt{!S!_r4_div}
}
@R@-stkimpl-def /mod 2 true %{
  !S!-int-div %arg{2} %arg{1}
  @R@-stack-PUSH1 %opt{!S!_r4_mod}
  @R@-stack-PUSH1 %opt{!S!_r4_div}
}
@R@-stkimpl-def mod 2 true %{
  !S!-int-mod %arg{2} %arg{1}
  @R@-stack-PUSH1 %opt{!S!_r4_mod}
}

@R@-stkimpl-def lshift 2 true %{
  !S!-int-lshift %arg{2} %arg{1}
  @R@-stack-PUSH1 %opt{!S!_r4_lshift}
}
@R@-stkimpl-def rshift 2 false %{
  @R@-stack-POPV %arg{1}
  try     %{ !S!-intlt0 %arg{1}
    @R@-stack-POPV %arg{2}
    @R@-stack-PUSH1 0
  } catch %{ "!S!-str-literal-0-%arg{1}"
    # leave int unchanged
  } catch %{
    @R@-stack-POPV %arg{2}
    decl -hidden int @R@_rshift_bits %arg{1}
    decl -hidden int @R@_rshift_int  %arg{2}
    try %{ !S!-intgt %opt{@R@_rshift_bits} 30  # lshift would be negative; anyway positive shifted has at most 31 bits
      @R@-stack-PUSH1 0
    } catch %{
      try %{ !S!-intlt0 %opt{@R@_rshift_int}  # flip MSB
        SETG+ @R@_rshift_bits -1
        @R@-stack-PUSH %opt{@R@_rshift_int} %opt{!S!_int32_min}
        @R@-impl-2/  # 2/ (but not int-div) is patched for !S!_int32_min dividend
        @R@-impl-+
        SLIST-POP-CHK-1_1 @R@_stack @R@_stk_arg
        SETG= @R@_rshift_int %opt{@R@_stk_arg1}
      }
      !S!-int-lshift 1 %opt{@R@_rshift_bits}
      !S!-int-div %opt{@R@_rshift_int} %opt{!S!_r4_lshift}
      @R@-stack-PUSH1 %opt{!S!_r4_div}
    }
  }
}

@R@-stkimpl-def 2* 1 true %{
  INT-ADDSUB -add %arg{1} %arg{1}
  @R@-stack-PUSH1 %opt{!S!_int}
}

@R@-stkimpl-def 2/ 1 true %{
  try %{ "!S!-str-literal-%opt{!S!_int32_min}-%arg{1}"
    @R@-stack-PUSH1 %opt{!S!_int31_min}
  } catch %{
    !S!-int-div %arg{1} 2
    @R@-stack-PUSH1 %opt{!S!_r4_div}
  }
}

decl -hidden str-list @R@_cmp_ops_slist = eq <> ne < lt > gt <= le >= ge
decl -hidden str @R@_def_int_ops_true  %{SETG= @R@_stack -1 %opt{@R@_stack}}
decl -hidden str @R@_def_int_ops_false %{SETG= @R@_stack  0 %opt{@R@_stack}}
DEFPOVH @R@-def-int-ops[..] %{
  try %{ !S!-nop-1_ %arg{@} } catch %{ fail !S!-stop }
  SETG- @R@_cmp_ops_slist %arg{1} %arg{2}
  @R@-stkimpl-def "0%arg{1}" 1 true "try %%{!S!-int%arg{2}0 %%arg{1};%opt{@R@_def_int_ops_true}} catch %%{%opt{@R@_def_int_ops_false}}"
}
try %{
  !S!-loop-16 eval %{@R@-def-int-ops %opt{@R@_cmp_ops_slist}}
} catch %{ !S!-err-eq !S!-stop }

decl -hidden str-list @R@_cmp_ops_slist = eq <> ne u< lt u> gt u<= le u>= ge
DEFPOVH @R@-def-int-op[2] %{
  @R@-stkimpl-def "%arg{1}" 2 true "try %%{!S!-int%arg{2} %%arg{2} %%arg{1};%opt{@R@_def_int_ops_true}} catch %%{%opt{@R@_def_int_ops_false}}"
}

@R@-def-int-op =  eq
@R@-def-int-op <> ne

@R@-stkimpl-def <  2 true %{
  @R@-helper-intcmp-g t %arg{1} %arg{2}
}
@R@-stkimpl-def <= 2 true %{
  @R@-helper-intcmp-g e %arg{1} %arg{2}
}
@R@-stkimpl-def >  2 true %{
  @R@-helper-intcmp-g t %arg{2} %arg{1}
}
@R@-stkimpl-def >= 2 true %{
  @R@-helper-intcmp-g e %arg{2} %arg{1}
}

@R@-stkimpl-def u<  2 true %{
  @R@-helper-intcmp-ug t %arg{1} %arg{2}
}
@R@-stkimpl-def u<= 2 true %{
  @R@-helper-intcmp-ug e %arg{1} %arg{2}
}
@R@-stkimpl-def u>  2 true %{
  @R@-helper-intcmp-ug t %arg{2} %arg{1}
}
@R@-stkimpl-def u>= 2 true %{
  @R@-helper-intcmp-ug e %arg{2} %arg{1}
}

@R@-stkimpl-def u. 1 true %{
  try %{ !S!-intge0 %arg{1}
    @R@-print "%arg{1} "
  } catch %{
    # add INT_MIN to make positive
    INT-ADDSUB-INTO -add %arg{1} -2147483648 !S!_r4_mod
    # now positive; compute x = 1000000000 * q + r
    # then add -INT_MIN with decimal unsigned arithmetic
    # x + -INT_MIN =  (q + 2) * 1000000000 + (r + 147483648)
    SETG= !S!_r4_div 2
    !S!-loop-2 eval %{  # q <= 2
      try %{ !S!-intgt %opt{!S!_r4_mod} 999999999
        SETG+ !S!_r4_div 1
        SETG+ !S!_r4_mod -1000000000
      }
    }
    SETG+ !S!_r4_mod 147483648
    !S!-loop-2 eval %{  # make r < 1000000000 again
      try %{ !S!-intgt %opt{!S!_r4_mod} 999999999
        SETG+ !S!_r4_div 1
        SETG+ !S!_r4_mod -1000000000
      }
    }
    @R@-print "%opt{!S!_r4_div}%opt{!S!_r4_mod} "
  }
}

# SETG+ @R@_boot_late_code : within over - >r - r> u< \;
decl -hidden int @R@_within_h_less_l
decl -hidden int @R@_within_t_less_l
@R@-stkimpl-def within 3 true %{
  INT-ADDSUB-INTO -remove %arg{1} %arg{2} @R@_within_h_less_l
  INT-ADDSUB-INTO -remove %arg{3} %arg{2} @R@_within_t_less_l
  @R@-stack-PUSH %opt{@R@_within_h_less_l} %opt{@R@_within_t_less_l}
  @R@-impl-u<
}

@R@-stkimpl-def negate  1 true %{
  !S!-int-neg %arg{1}
  @R@-stack-PUSH1 %opt{!S!_int}
}
@R@-stkimpl-def dnegate 2 true %{
  !S!-int-neg %arg{2}
  @R@-stack-PUSH1 %opt{!S!_int}
  INT-ADDSUB -remove -1 %arg{1}
  @R@-stack-PUSH1 %opt{!S!_int}
}

@R@-stkimpl-def min 2 true %{
  try %{
    !S!-intlt-w %arg{1} %arg{2}
    @R@-stack-PUSH1 %arg{1}
  } catch %{
    @R@-stack-PUSH1 %arg{2}
  }
}
@R@-stkimpl-def max 2 true %{
  try %{
    !S!-intlt-w %arg{1} %arg{2}
    @R@-stack-PUSH1 %arg{2}
  } catch %{
    @R@-stack-PUSH1 %arg{1}
  }
}

@R@-stkimpl-def abs 1 false %{
  try %{ INTGT0 %arg{1}
  } catch %{
    @R@-impl-negate
  }
}
@R@-stkimpl-def dabs 2 false %{
  try %{ !S!-intge0 %arg{1}
  } catch %{
    @R@-impl-dnegate
  }
}

@R@-stkimpl-def and 2 true %{
  try %{
    "@R@-and-handle-%arg{1}-%arg{2}"
  } catch %{
    @R@-stack-PUSH1 %sh{echo $(($1 & $2))}
  }
}
@R@-stkimpl-def or  2 true %{
  try %{
    "@R@-or-handle-%arg{1}-%arg{2}"
  } catch %{
    @R@-stack-PUSH1 %sh{echo $(($1 | $2))}
  }
}
@R@-stkimpl-def xor 2 true %{
  @R@-stack-PUSH1 %sh{echo $(($1 ^ $2))}
}
DEFPOVH @R@-and-handle--1--1[0] %{
  @R@-stack-PUSH1 -1
}
DEFPOVH @R@-and-handle--1-0 [0]  %{
  @R@-stack-PUSH1 0
}
DEFPOVH @R@-and-handle-0--1 [0]  %{
  @R@-stack-PUSH1 0
}
DEFPOVH @R@-and-handle-0-0  [0]   %{
  @R@-stack-PUSH1 0
}
DEFPOVH @R@-or-handle--1--1 [0] %{
  @R@-stack-PUSH1 -1
}
DEFPOVH @R@-or-handle--1-0  [0] %{
  @R@-stack-PUSH1 -1
}
DEFPOVH @R@-or-handle-0--1  [0] %{
  @R@-stack-PUSH1 -1
}
DEFPOVH @R@-or-handle-0-0   [0] %{
  @R@-stack-PUSH1 0
}


@R@-stkimpl-def d0=  2 true %{
  try %{
    "!S!-literal-0-%arg{1}"
    "!S!-literal-0-%arg{2}"
    @R@-stack-PUSH1 0
  } catch %{
    @R@-stack-PUSH1 -1
  }
}
@R@-stkimpl-def d0<>  2 true %{
  try %{
    "!S!-literal-0-%arg{1}"
    "!S!-literal-0-%arg{2}"
    @R@-stack-PUSH1 -1
  } catch %{
    @R@-stack-PUSH1  0
  }
}

@R@-stkimpl-def d=   4 true %{
  try %{
    !S!-inteq %arg{1} %arg{3}
    !S!-inteq %arg{2} %arg{4}
    @R@-stack-PUSH1 -1
  } catch %{
    @R@-stack-PUSH1  0
  }
}
@R@-stkimpl-def d<>  4 true %{
  try %{
    !S!-inteq %arg{1} %arg{3}
    !S!-inteq %arg{2} %arg{4}
    @R@-stack-PUSH1  0
  } catch %{
    @R@-stack-PUSH1 -1
  }
}

@R@-stkimpl-def d0>= 2 true %{
  try %{ !S!-intge0 %arg{1}
    @R@-stack-PUSH1 -1
  } catch %{
    @R@-stack-PUSH1  0
  }
}
@R@-stkimpl-def d0<  2 true %{
  try %{ !S!-intge0 %arg{1}
    @R@-stack-PUSH1  0
  } catch %{
    @R@-stack-PUSH1 -1
  }
}
@R@-stkimpl-def d0>  2 true %{
  try %{ INTGT0 %arg{1}
      @R@-stack-PUSH1 -1
  } catch %{
    "!S!-literal-0-%arg{1}"
    try %{ INTGT0 %arg{2}
      @R@-stack-PUSH1 -1
    } catch %{
      @R@-stack-PUSH1  0
    }
  } catch %{
      @R@-stack-PUSH1  0
  }
}
@R@-stkimpl-def d0<= 2 true %{
  try %{ INTGT0 %arg{1}
      @R@-stack-PUSH1  0
  } catch %{
    "!S!-literal-0-%arg{1}"
    try %{ INTGT0 %arg{2}
      @R@-stack-PUSH1  0
    } catch %{
      @R@-stack-PUSH1 -1
    }
  } catch %{
      @R@-stack-PUSH1 -1
  }
}

@R@-stkimpl-def s>d 1 false %{
  try %{ !S!-intge0 %arg{1}
    @R@-stack-PUSH1  0
  } catch %{
    @R@-stack-PUSH1 -1
  }
}
@R@-stkimpl-def d>s 1 true %{
  # = DROP
}

# helpers

DEFPOVH @R@-helper-intcmp-g[3] -docstring %{
  Signed intgt | intge
  Args: [t|e] INT1 INT2
} %{
  try %{
    "!S!-intg%arg{1}-w" %arg{2} %arg{3}
    @R@-stack-PUSH1 -1
  } catch %{
    @R@-stack-PUSH1  0
  }
}
DEFPOVH @R@-helper-intcmp-ug[3] %{
  decl -hidden int @R@_helper_intcmp_ug1 %arg{2}
  decl -hidden int @R@_helper_intcmp_ug2 %arg{3}
  SETG+            @R@_helper_intcmp_ug1 %opt{!S!_int32_min}
  SETG+            @R@_helper_intcmp_ug2 %opt{!S!_int32_min}
  @R@-helper-intcmp-g %arg{1} %opt{@R@_helper_intcmp_ug1} %opt{@R@_helper_intcmp_ug2}
}
